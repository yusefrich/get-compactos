{{-- <div data-aos="fade-up" style="background: linear-gradient(0deg, rgba(17, 17, 17, 0.5), rgba(17, 17, 17, 0.5)), url('{{url('assets_front/imgs/banner-smaller-bg.jfif')}}');"  --}}
<div data-aos="fade-up" style="background:  url('{{url('assets_front/imgs/bg-em-breme-correto.jpg')}}');" 
    class="container-fluid banner-smaller text-center">
    <img class="icon-emp" src="{{url('assets_front/imgs/icon-building.svg')}}" alt="">
    {{-- style="background-image: url('{{url('/storage/home/'.$img->img)}}')" --}}
    <h3 class="d-none d-md-block">Novidades em breve</h3>
    <h3 class="d-md-none mob-h1">Novidades em breve</h3>
    <p class="text-white mr-auto ml-auto" style="max-width: 496px;">
        Desenhar o futuro, construir o agora com inovação e autenticidade. Em breve mais compactos inteligentes para você. 
    </p>
</div>