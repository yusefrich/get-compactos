<div style="background:linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url('{{url('assets_front/imgs/monte-smaller-bg.jpg')}}');
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            position: relative;
            width: 100%;" class="banner-smaller-height d-print-none">
    <div class="bg-banner-black d-none d-md-block"></div>

    <div class=" v-center v-mob-normal container-xl mr-auto ml-auto">
        <div data-aos="fade-up" class="social-banner text-center d-none d-md-block">
            <p class="mt-5">083 3031-9191</p>
            <a style="padding: 6px" class="btn btn-light btn-round m-8" target="_blank" href="tel:83-3031-9191">
                {{-- <ion-icon style="font-size: 24px;" name="call"></ion-icon> --}}
                <img src="{{asset('assets_front/imgs/phone-icon.svg')}}" alt="phone icon">
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos.">
                <ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon>
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://www.instagram.com/getcompactos/">
                <ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon>
            </a>
        </div>
        <div class="d-flex justify-content-center">

            <div data-aos="fade-right"  style="max-width: 541px;" class="mt-md-170">
                <h1  style="max-width: 541px;" class="text-white text-center text-md-left mx-auto mx-md-0">
                    Monte o compacto dos seus sonhos
                </h1>
                <p  style="max-width: 427px;" class="text-white text-center text-md-left mx-auto mx-md-0">
                    Monte a unidade que combina com seu estilo de vida, com a sua personalidade e que cabe no seu bolso.
                </p>
                <div class="d-flex">
                    <a href="#pageTop" onclick="setIsClient(true)" id="btnClient" class="btn btn-outline-primary mr-2">Sou cliente Get</a>
                    <a href="#pageTop" onclick="setIsClient(false)" id="btnNotClient" class="btn btn-outline-primary">Quero meu Get</a>
                </div>
            </div>
        </div>
        <div class=" d-md-none">

            <div class="d-flex justify-content-center pb-20">
                <div class="mt-40 mb-50">
                    <p  class="text-white mb-0 ml-2">|</p>
                    <ion-icon style="font-size: 22px" class="text-white" name="chevron-down-outline"></ion-icon>
                </div>
            </div>
        </div>

    </div>

</div>

@push('scripts')
<script>

</script>
@endpush