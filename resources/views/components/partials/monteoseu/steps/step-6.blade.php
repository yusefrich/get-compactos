@foreach($data as $key => $emp)
@foreach($emp['floors'] as $key => $flor)
@foreach($flor['unities'] as $florkey => $uni)
@foreach($uni as $unikey => $singleUni )
{{-- <div id="collapse-electros-{{$flor['id']}}-{{$singleUni['id']}}" class="d-none">
    <div class="text-center mx-4 mx-md-0">
        <img src="{{url('assets_front/imgs/television.svg')}}" class="pb-2" alt="">
        <h4  class="text-preto-azulado-get">
            Lista de eletrodomésticos
        </h4>
        @if($singleUni['electros_new'] != null)
        <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
            Selecione o projeto de móveis planejados e siga para a próxima etapa 
        </p>
        @else
        <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
            Essa unidade não possui Eletrodomésticos cadastrados 
        </p>

        @endif
    </div>
    <div class="container-small mx-auto">
        <div class="row mb-40">
            @if($singleUni['electros_new'] != null)
            @foreach ($singleUni['electros_new'] as $su)
                <div class="col-md-6 col-right mb-20">
                    <div style="width: 115px" class="custom-control custom-checkbox get-checkbox">
                        <input onclick="setMovel({{json_decode($su)->price}}, '{{json_decode($su)->name}}', this)"style="box-shadow: none" type="checkbox" class="custom-control-input" aria-describedby="movel-value-{{json_decode($su)->id}}" id="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                        <label class="custom-control-label text-preto-azulado-get" for="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">{{json_decode($su)->name}}</label>
                        <small id="movel-value-{{json_decode($su)->id}}" class="form-text text-vermelho-get">
                            {{'R$ '.number_format(json_decode($su)->price, 2, ',', '.')}}
                        </small>
                    </div>
                </div>
            @endforeach
            @endif
        </div>
    </div>
</div> --}}


<div id="collapse-electros-{{$flor['id']}}-{{$singleUni['id']}}" class="row d-none">
    <div data-aos="fade-right" class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 6 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Que tal uns eletrodomésticos para seu apartamento?

            </h4>
            <p style="max-width: 360px;" class="caption-15 text-preto-azulado-get mb-5">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>

            @if($singleUni['electros_new'] != null)
            @foreach ($singleUni['electros_new'] as $su){{-- {{json_decode($su)->name}} --}}
                <div class="mb-20">
                    <div class="custom-control custom-checkbox get-checkbox">
                        <input onclick="setMovel({{json_decode($su)->price}}, '{{json_decode($su)->name}}', this)"style="box-shadow: none" type="checkbox" class="custom-control-input" aria-describedby="movel-value-{{json_decode($su)->id}}" id="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                        <label class="d-flex custom-control-label text-preto-azulado-get" for="forniture-select-check{{$flor['id']}}-{{$singleUni['id']}}-{{json_decode($su)->id}}">
                            {{json_decode($su)->name}}
                            <small id="movel-value-{{json_decode($su)->id}}" class="form-text text-vermelho-get ml-2">
                                + {{'R$ '.number_format(json_decode($su)->price, 2, ',', '.')}}
                            </small>
                        </label>
                    </div>
                </div>
            @endforeach
            @endif
    
        </div>
    </div>
    <div class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_electros_detail')" id="current-electros-select-area"
            class="item-detail-img mr-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-elec.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4 class="ml-32 text-middle-gray  value-total-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>


@endforeach
@endforeach
@endforeach
@endforeach

