<div id="emp-{{$emp['id']}}" class="row d-none step-1-row">
    <div class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 2 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Escolha o pavimento
            </h4>
            <p style="max-width: 360px;" class="caption-15 text-preto-azulado-get mb-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu
                sagittis. </p>

            @foreach($emp['floors'] as $subkey => $flor)
            <div data-aos="fade" class="builder-card  pb-3">
                <div class="d-flex">
                    <div class="custom-control check-emp-offset custom-radio get-select">
                        <input
                            style="box-shadow: none" type="radio" onclick="selectFloor({{json_encode($flor)}})" name="florSelectRadio" class="custom-control-input get-check-emp"
                            aria-describedby="movel-value-" id="flor-check-{{$flor['id']}}">
                        <label class="custom-control-label text-preto-azulado-get" for="flor-check-{{$flor['id']}}">

                            <p  class="no-wrap-text click-link text-preto-azulado-get caption-16 mb-0">{{$flor['name']}} <span id="smallest-pav-{{$emp['id']}}-{{$flor['id']}}" class="caption-14 text-hex-797979">A partir de R$159.000,00</span> </p>
                        </label>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>
    <div class="col-md-7  item-detail-col">{{-- value-total-class --}}
        <div onclick="openModalItemDetail('#modal_floor_detail')" id="current-flor-select-area"
            class="item-detail-img mr-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-pav.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4 class="ml-32 text-middle-gray  value-total-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
    <div id="unity_list_{{$key}}" class="col-md-12">
        @foreach($emp['floors'] as $subkey => $flor)

        <div class="text-center planta-thumb mb-40 collapse collapse-pav-reset collapse-pav-map"
            id="col-pav-{{$subkey}}">

            <map name="map_unities_{{$flor['id']}}">
                @foreach($flor['unities'] as $unityKey => $plantas)
                @if($plantas[0]['floor_id'] == $flor['id'])

                @foreach($plantas as $singleKey => $unity)
                <area target="" alt="" title="" @if ($unity['sold']==0) class="select-unidade-click"
                    data-value="{{$unity['price']}}" onclick="toggleUnityByMap(
                                                    '#col-planta-{{$flor['id']}}-{{ $plantas[0]['size_slug']}}', 
                                                    '#radio-uni-{{$flor['id']}}-{{$unity['id']}}', 
                                                    '{{url('storage/unity/'.$unity['img'])}}', 
                                                    {{json_encode($unity['planned'])}}, 
                                                    {{json_encode($flor)}}, 
                                                    {{json_encode($unity)}}
                                                    )" @endif coords="{{$unity['coordinate']}}" shape="poly">
                @endforeach

                @endif
                @endforeach
            </map>

            <p class="caption-20 text-preto-azulado-get mb-20">{{$flor['name']}}</p>
            <img usemap="#map_unities_{{$flor['id']}}" style="margin-bottom: 15px"
                class="img-fluid @if($subkey == 0) img-map-styles @endif" src="{{url('storage/floor/'.$flor['img'])}}"
                class="pb-2" alt="">
            <br>
            <button type="button" class="btn btn-outline-dark" data-toggle="modal"
                data-target="#modal-floors-{{$subkey}}">Ampliar imagem</button>
        </div>
        @endforeach
    </div>
    <div class="col-md-12">
        @foreach($emp['floors'] as $subkey => $flor)
        @foreach($flor['unities'] as $unityKey => $plantas)

        @if($plantas[0]['floor_id'] == $flor['id'])

        <div class="collapse collapse-reset" id="col-planta-{{$flor['id']}}-{{$plantas[0]['size_slug']}}">
            <div class="row">
                <div class="col-5 col-md-4 text-right">
                    <p class="caption-20 text-preto-azulado-get mb-0 mr-3">{{$plantas[0]['size']}}</p>
                    <img style="margin-bottom: 15px" class="img-fluid img-planta-uni uni-outline img-detail"
                        src="{{url('assets_front/imgs/planta-unidade.png')}}" class="pb-2" alt="">
                </div>
                <div class="col-7 pl-0 col-md-8 mt-4 mt-md-0">

                    <div class="row ">
                        @foreach($plantas as $singleKey => $unity)
                        <div class="col-md-6 mb-18 @if ($unity['sold'] != 1) get-free-select @endif">
                            <div class="custom-control custom-radio custom-control-inline get-select">
                                <input @if ($unity['sold']==1) disabled @endif
                                    onclick="setUnity('{{url('storage/unity/'.$unity['img'])}}', {{json_encode($unity['planned'])}}, {{json_encode($flor)}}, {{json_encode($unity)}}, 'uni-label-{{$flor['id']}}-{{$unity['id']}}')"
                                    type="radio" id="radio-uni-{{$flor['id']}}-{{$unity['id']}}"
                                    name="radio-uni-{{$flor['id']}}-{{$unity['size']}}" data-value="{{$unity['price']}}"
                                    class="@if ($unity['sold'] == 1) get-sold-input @endif custom-control-input select-unidade">
                                <label class="get-unity-lavel custom-control-label uni-label" id="uni-label-{{$flor['id']}}-{{$unity['id']}}"
                                    for="radio-uni-{{$flor['id']}}-{{$unity['id']}}">{{$unity['name']}} @if ($unity['sold'] == 1) <small class="text-red get-free-soldLabel">(vendido)</small> @endif</label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        @endif

        @endforeach
        @endforeach

    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_floor_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <img class="modal-item-img" id="modal_floor_detail-img" src="" alt="">

            </div>
        </div>
    </div>
</div>

@endpush

@push('scripts')
<script>
    var previousMapUnitySelect = "";
    function selectFloor(floor) {
        $('#disabled-uni-selec').removeAttr("hidden");

        /* console.log(floor); */
        setItemMOS("#current-flor-select-area", "#modal_floor_detail-img", `{{url('storage/floor/${floor.img}')}}`);
        var uniHtml = '';
        var plantaIndexInside = 0;
        Object.entries(floor.unities).forEach(element => {/*  */
            uniHtml = uniHtml+
            '<div data-aos="fade" class="builder-card  pb-3">'+
                `<div class="d-flex">`+
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input onclick='selectUnity(${JSON.stringify(element[1])}, ${JSON.stringify(floor)})' style="box-shadow: none" type="radio"  name="unitySelectRadio" class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="unity-check-${element[0]}">`+
                            `<label class="click-link custom-control-label text-preto-azulado-get" for="unity-check-${element[0]}">`+
                                `<p  class="text-nowrap text-preto-azulado-get caption-16 mb-0">${element[0]}<span id="smallest-planta-${floor.id}-${element[1][0].size_slug}" class="caption-14 text-hex-797979 ml-2">teste</span></p>`+
                            `</label>`+
                        `</div>`+
                    `</div>`+
            '</div>';
            plantaIndexInside++;
        });

        $('#disabled-uni-selec').removeAttr("disabled");
        $('#unities-select-area').html(uniHtml);
    }
    function toggleUnityByMap(unityGroupId, unityRadioId, setUnityPath, setUnityPlannedPath, setUnityFloor, setUnityUnity) {

        if (unityGroupId != previousMapUnitySelect){
            resetCollapse()

            previousMapUnitySelect = unityGroupId;
            $(previousMapUnitySelect).collapse('show');
        }

        $(unityRadioId).prop("checked", true);
        setUnity(setUnityPath, setUnityPlannedPath, setUnityFloor, setUnityUnity, 'uni-label-'+setUnityFloor.id+'-'+setUnityUnity.id);

    }
    function toggleUnityByMenu(unityGroupId, caller, scrolId) {
        $('html, body').animate({
            scrollTop: $(scrolId).offset().top
        }, 100);

        if (unityGroupId != previousMapUnitySelect){
            resetCollapse()

            $(".click-link-unities").each(function () {  
                $(this).removeClass("active-unity");
            })
                $(caller).addClass("active-unity");

            previousMapUnitySelect = unityGroupId;
            $(previousMapUnitySelect).collapse('show');
        }
    }
    $('.modal_img_emp').on('shown.bs.modal', function () {
        resizeMap();
    })

    $('.collapse-pav-map').each(function( index ) {
            $(this).on('shown.bs.collapse', function () {
                
                resizeMap();
                /* $('.img-map-styles').maphilight(); */

                /* $('.mapping').mouseover(function() {
                    alert($(this).attr('id'));
                }).mouseout(function(){
                    alert('Mouseout....');      
                }); */
            })
        });

        
        function resetCollapse(){
            $('.collapse-reset').each(function( index ) {
                $( this ).collapse('hide');
            });
        }
        var currentPavBtn;
        var oldBtbArrow;
        function resetPavmentoCollapse(btnArrowStr, event){
            $('.collapse-pav-reset').each(function( index ) {
                $( this ).collapse('hide');
            });
            if(oldBtbArrow == btnArrowStr){
                $('.' + btnArrowStr).each(function( index ) {
                    $( this ).toggleClass("d-none");
                });
            }else {
                $('.' + btnArrowStr).each(function( index ) {
                    $( this ).toggleClass("d-none");
                });
                $('.' + oldBtbArrow).each(function( index ) {
                    $( this ).toggleClass("d-none");
                });

            }
            oldBtbArrow = btnArrowStr;

            $( ".btn-select-pav" ).each(function( index ) {
                $( this ).removeClass("active")
            });

            if($(event).hasClass("active")){
                $(event).toggleClass("active");
            }else {
                $(currentPavBtn).removeClass("active");
                $(event).addClass("active");
            }
            currentPavBtn = event;
        }

        function setUnity(path, plannedPath, floor, unity, labelId){
            oportunity.unity.floor = floor;
            oportunity.unity.data = unity;
            /* console.log("checking the setUnity");
            console.log(unity.id); */
            


            setImg(path, plannedPath);

            $(oportunity.appliances.htmlId).addClass("d-none");
            var idString = "#collapse-electros-"+floor.id+"-"+unity.id;

            oportunity.appliances.htmlId = idString;

            $(oportunity.appliances.htmlId).removeClass("d-none");
            console.log(oportunity.appliances.htmlId);

            $(".uni-label").each(function () {  
                $(this).removeClass("active-unity");
            })
            $("#"+labelId).addClass("active-unity");
        }
        function setImg(path, plannedPath){
            setPlanned(plannedPath);
            $('.img-detail').each(function() {
                $( this ).attr("src",path);
            });
        }

        function setPlanned(obj){
            console.log(obj);
            var newHtml = "";
            var newHtmlMob = "";
            var hasPlans = false;
            obj.forEach(e => {
                hasPlans = true;
                var eStringify = JSON.stringify(e);
                console.log("plan shit");
                console.log(eStringify);

                newHtml = newHtml + 
                '<div data-aos="fade" class="builder-card  pb-3">'+
                    `<div class="d-flex">`+
                            `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                                `<input onclick='selectPlan("${e.name}", ${e.id}, ${e.price}, ${JSON.stringify(e)} )' style="box-shadow: none" type="radio"  name="planSelectRadio" class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="plan-check-${e.id}">`+
                                `<label class="click-link custom-control-label text-preto-azulado-get" for="plan-check-${e.id}">`+
                                    `<p  class="no-wrap-text text-preto-azulado-get caption-16 mb-0">${e.name} <span class="text-hex-797979 caption-14">+ ${e.price}</span></p>`+
                                `</label>`+
                            `</div>`+
                        `</div>`+
                '</div>'

                newHtmlMob = newHtmlMob + 
                '<div data-aos="fade" class="builder-card  pb-3">'+
                    `<div class="d-flex">`+
                            `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                                `<input onclick='selectPlan("${e.name}", ${e.id}, ${e.price}, ${JSON.stringify(e)} )' style="box-shadow: none" type="radio"  name="planSelectRadio" class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="plan-check-${e.id}">`+
                                `<label class="click-link custom-control-label text-preto-azulado-get" for="plan-check-${e.id}">`+
                                    `<p  class="no-wrap-text text-preto-azulado-get caption-16 mb-0">${e.name} <span class="text-hex-797979 caption-14">+ ${e.price}</span></p>`+
                                `</label>`+
                            `</div>`+
                        `</div>`+
                '</div>'
            });

            //* adicionando opcao "nenhum"
            newHtml = newHtml + 
            '<div data-aos="fade" class="builder-card  pb-3">'+
                `<div class="d-flex">`+
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input onclick="selectPlan('nenhum', '-1', '0', '')" style="box-shadow: none" type="radio"  name="planSelectRadio" class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="plan-check--1">`+
                            `<label class="click-link custom-control-label text-preto-azulado-get" for="plan-check--1">`+
                                `<p  class="no-wrap-text text-preto-azulado-get caption-16 mb-0">Nenhum </p>`+
                            `</label>`+
                        `</div>`+
                    `</div>`+
            '</div>'

            newHtmlMob = newHtmlMob + 
            '<div data-aos="fade" class="builder-card  pb-3">'+
                `<div class="d-flex">`+
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input onclick="selectPlan('nenhum', '-1', '0', '')" style="box-shadow: none" type="radio"  name="planSelectRadio" class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="plan-check--1">`+
                            `<label class="click-link custom-control-label text-preto-azulado-get" for="plan-check--1">`+
                                `<p  class="no-wrap-text text-preto-azulado-get caption-16 mb-0">Nenhum </p>`+
                            `</label>`+
                        `</div>`+
                    `</div>`+
            '</div>'

            /* if(hasPlans){
                $("#planejados-desc").html(
                    "Selecione o projeto de móveis planejados e siga para a próxima etapa"
                );
            }else {
                $("#planejados-desc").html(
                    "Essa unidade não possui Móveis planejados cadastrados"
                );
            } */
            $("#plan-select-area").html(
                newHtml
            );
            /* $(".plan-select-area").html(
                newHtmlMob
            ); */
        }

</script>
@endpush