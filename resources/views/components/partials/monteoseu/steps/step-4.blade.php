<div class="row">{{-- unities --}}
    <div data-aos="fade-right" class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 4 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Escolha o apartamento
            </h4>
            <p style="max-width: 360px;" class="caption-15 text-preto-azulado-get mb-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>

            <div id="ap-select-area">
            </div>

        </div>
    </div>
    <div class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_ap_detail')" id="current-ap-select-area"
            class="item-detail-img mr-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-ap.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4 class="ml-32 text-middle-gray  value-total-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_ap_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <img class="modal-item-img" id="modal_ap_detail-img" src="" alt="">

            </div>
        </div>
    </div>
</div>
@endpush
@push('scripts')
    <script>
        function selectAp(ap, floor, unity) {
            $('#disabled-ap-selec').removeAttr("hidden");

            console.log("selected-ap to set unity");
            console.log(ap);
            console.log(unity);
            setItemMOS("#current-ap-select-area", "#modal_ap_detail-img", `{{url('storage/unity/${ap.img}')}}`);
            updateValueUnity(ap.price);
            setUnity(`{{url('storage/unity/${ap.img}')}}`, ap.planned, floor, unity, `uni-label-${floor.id}-${unity.id}`);
        }
    </script>
@endpush