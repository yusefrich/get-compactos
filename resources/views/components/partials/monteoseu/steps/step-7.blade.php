{{-- <div class="text-center mx-4 mx-md-0">
    <img src="{{url('assets_front/imgs/payment.svg')}}" class="pb-2" alt="">
    <h4 class="text-preto-azulado-get">
        Sugestão de pagamento
    </h4>
    <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
        Informe as suas sugestões de pagamento e siga para a próxima etapa
    </p>
</div> --}}

<div class="row">
    <div data-aos="fade-right" class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 7 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Sugestão de pagamento
            </h4>
            <p style="max-width: 360px;" class="caption-15 text-preto-azulado-get mb-5">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>
            @foreach($data as $keyEmpPayment => $emp)
            @foreach($emp['payments'] as $keyPayment => $payment)
                <div class="custom-control px-md-3 mb-2 custom-radio get-select">
                    <input onclick="selectPaymentSugestionScroll({{json_encode($payment)}}, true)" @if($keyPayment == 0) checked="checked" @endif type="radio" id="paymentSugRadio-{{$payment['id']}}" name="paymentSugRadio" class="custom-control-input">
                    <label style="color: #111111"   class="d-flex custom-control-label text-preto-azulado-get" for="paymentSugRadio-{{$payment['id']}}">
                            {{$payment['title']}} 
                            <small  class=" text-vermelho-get ml-2">{{$payment['desc']}}</small>
                    </label>
                    
                </div>
            @endforeach
            @endforeach
            {{-- //* ESPAÇO PARA O USUÁRIO SELECIONAR [NENHUM] --}}
            <div class="custom-control px-md-3 mb-2 custom-radio get-select">
                <input onclick="selectPaymentSugestionScroll('', false)"type="radio" id="paymentSugRadio--1" name="paymentSugRadio" class="custom-control-input">
                <label style="color: #111111"   class="d-flex custom-control-label text-preto-azulado-get" for="paymentSugRadio--1">
                        Nenhum
                </label>
                
            </div>

        </div>
    </div>
    <div class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_payment_detail')" id="current-payment-select-area"
            class="item-detail-img mr-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-pagamento.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4 class="ml-32 text-middle-gray mb-4 value-total-class font-weight-bold"></h4>

            {{-- <p id="current-payment-detail" class="caption-16 text-white item-detail-detail"></p>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt=""> --}}
            <div id="payment-data-holder" class="d-none">
                <div class="payment-label">
                    <p class="caption-14  text-middle-gray ml-32 mb-0">Entrada </p>
                    <p style="font-weight: 500;" id="payment-entrada"
                    class="caption-18 text-middle-gray ml-32 font-weight-bold mb-0">20.000,00</p>
                    <div id="get-percent-entrada-holder" style="margin-bottom: 1px;" class="percent-filler text-right mt-2 percent-holder-offset mb-4">
                        <p id="get-percent-entrada" class="mb-0 caption-16 text-preto-azulado-get d-inline-block">50%</p>
                    </div>
                </div>
                <div class="payment-label mt-2">
                    <p class="caption-14  text-middle-gray ml-32 mb-0">Semestrais </p>
                    <p style="font-weight: 500;" id="payment-semestrais"
                    class="caption-18 text-middle-gray  ml-32 font-weight-bold mb-0">20.000,00</p>
                    <div id="get-percent-semestrais-holder" style="margin-bottom: 1px" class="percent-filler text-right mt-2 percent-holder-offset mb-4 ">
                        <p id="get-percent-semestrais" class="mb-0 caption-16 text-preto-azulado-get d-inline-block">50%</p>
                    </div>
                </div>
                <div class="payment-label mt-2">
                    <p class="caption-14  text-middle-gray ml-32 mb-0">Mensais </p>
                    <p style="font-weight: 500;" id="payment-mensais"
                    class="caption-18 text-middle-gray  ml-32 font-weight-bold mb-0">20.000,00</p>
                    <div id="get-percent-mensais-holder" style="margin-bottom: 1px" class="percent-filler text-right mt-2 percent-holder-offset mb-4">
                        <p id="get-percent-mensais" class="mb-0 caption-16 text-preto-azulado-get d-inline-block">50%</p>
                    </div>        
                </div>

                <div class="payment-label mt-2">
                    <p class="caption-14  text-middle-gray ml-32 mb-0">Financiamento </p>{{-- <span>i</span> --}}
                    <p style="font-weight: 500;" id="payment-financiamento"
                    class="caption-18 text-middle-gray  ml-32 font-weight-bold mb-0">20.000,00</p>
                    <div id="get-percent-financiamento-holder" style="margin-bottom: 1px" class="percent-filler text-right mt-2 percent-holder-offset mb-4">
                        <p id="get-percent-financiamento" class="mb-0 caption-16 text-preto-azulado-get d-inline-block">50%</p>
                    </div>

                </div>



            </div>
    
            <div id="none-payment-info" class="container-smaller mx-auto d-none" > {{-- collapse --}} {{-- id="playmentCustomCollapse" --}}
                <div class="text-center">
                    <p class="mob-paragraph-16 text-preto-azulado-get mx-auto mb-40" style="max-width: 380px;">
                        Para outra condição de pagamento, enviar a proposta customizada na próxima etapa.
                    </p>    
                </div>
            </div>
            
        </div>
    </div>
</div>

@foreach($data as $keyEmpPayment => $emp)
<div id="selectPayment-{{$emp['id']}}" class="text-center mb-72 mb-mob-30 collapse paymentSugestionToggle">
<div class="row">
    {{-- <div class="dropdown">
        <button class="btn btn-outline-dark btn-smaller dropdown-toggle" type="button" id="dropdownPaymentSug" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            selecione o tipo
        </button>
        <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
            @foreach($emp['payments'] as $keyPayment => $payment)
            <button class="dropdown-item get-dropdown" onclick="selectPaymentSugestion({{json_encode($payment)}})" >{{$payment['title']}}</button>
            @endforeach
            <button class="dropdown-item get-dropdown" onclick="selectPaymentCustom()">Customizada</button>
        </div>
    </div> --}}
    @foreach($emp['payments'] as $keyPayment => $payment)
        
    <div class="col-md-4 mb-28 ">
        {{-- <button class="dropdown-item get-dropdown" onclick="selectPaymentSugestion({{json_encode($payment)}})" >{{$payment['title']}}</button> --}}
        <div class="custom-control px-md-3 custom-radio get-select">
            <input onclick="selectPaymentSugestionScroll({{json_encode($payment)}})" @if($keyPayment == 0) checked="checked" @endif type="radio" id="paymentSugRadio-{{$payment['id']}}" name="paymentSugRadio" class="custom-control-input">
            <label style="color: #111111" class="custom-control-label text-preto-azulado-get" for="paymentSugRadio-{{$payment['id']}}">{{$payment['title']}}</label>
            <small class="form-text text-vermelho-get">{{$payment['desc']}}</small>
        </div>
    </div>
    @endforeach

    {{-- <div class="col-md-4">
        
        <div class="custom-control custom-radio">
            <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
            <label class="custom-control-label" for="customRadio1">Toggle this custom radio</label>
        </div>
        <div class="custom-control custom-radio">
            <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
            <label class="custom-control-label" for="customRadio2">Or toggle this other custom radio</label>
        </div>

        <button class="btn btn-outline-dark btn-smaller dropdown-toggle" type="button" id="dropdownPaymentSug" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            selecione o tipo
        </button>
        <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">
            @foreach($emp['payments'] as $keyPayment => $payment)
            <button class="dropdown-item get-dropdown" onclick="selectPaymentSugestion({{json_encode($payment)}})" >{{$payment['title']}}</button>
            @endforeach
            <button class="dropdown-item get-dropdown" onclick="selectPaymentCustom()">Customizada</button>
        </div>
    </div> --}}
</div>
</div>
@endforeach 


@push('scripts')
    <script>
        function selectPaymentSugestionScroll(selected, showingData) {
            /* scrollToId("#sec-step-5-end"); */
            $('#disabled-pay-selec').removeAttr("hidden");

            console.log("data on selectPaymentSugestion");
            console.log(selected);
            if(showingData){
                $("#payment-data-holder").removeClass("d-none");
                $("#none-payment-info").addClass("d-none");
                setNullRangeValue(false);
                selectPaymentSugestion(selected);
            }else {
                $("#payment-data-holder").addClass("d-none");
                $("#none-payment-info").removeClass("d-none");
                setNullRangeValue(true);
            }

            $("#current-payment-select-area").css("background-image", "none");
            
        }
        function selectPaymentSugestion(selected) {
            //console.log("selected")
            //console.log(selected)
            //console.log("oportunity")
            //console.log(oportunity)

            setRangeValue(selected);

            $('#dropdownPaymentSug').html(selected.title);
            $('#playmentSliderCollapse').collapse('show');
            $('#playmentCustomCollapse').collapse('hide');

            $('#payment-op-print').removeClass('d-none');
            oportunity.payment.custom = false;
            
        }
        function selectPaymentCustom(selected) {
            $('#payment-op-print').addClass('d-none');
            oportunity.payment.custom = true;

            $('#dropdownPaymentSug').html("Customizada");
            $('#playmentSliderCollapse').collapse('hide');
            $('#playmentCustomCollapse').collapse('show');
            
        }
        function setNullRangeValue(value){
            oportunity.payment.isNull = value;
        }
        function setRangeValue(value){
            //calculo da quantidade de messes
            var monthsForDeliver = diff_months(oportunity.development.order_day, oportunity.development.deliver_day)
            var semesterForDeliver = Math.round(monthsForDeliver / 6);

            

            oportunity.payment.semesterTimes = semesterForDeliver;
            oportunity.payment.montlyTimes = monthsForDeliver;
            
            oportunity.payment.typeTitle = value.title;
            oportunity.payment.typeDesc = value.desc;
            //debugs de teste
            /* console.log("months for delivery");
            console.log("order");
            console.log(oportunity.development.order_day);
            
            console.log("delivery");
            console.log(oportunity.development.deliver_day);

            console.log("diff");
            console.log(monthsForDeliver);
            
            console.log("quant semestres");
            console.log(semesterForDeliver); */

            //get and set the range values with the payment sugestions
            console.log("dados de setRangeValue");
            oportunity.payment.entry = +((value.entry * +oportunity.total)/100).toFixed(2);
            oportunity.payment.semester = +(((value.semester * +oportunity.total)/100)/semesterForDeliver).toFixed(2);
            oportunity.payment.montly = +(((value.montly * +oportunity.total)/100)/monthsForDeliver).toFixed(2);
            oportunity.payment.semesterTotal = +((value.semester * +oportunity.total)/100).toFixed(2);
            oportunity.payment.montlyTotal = +((value.montly * +oportunity.total)/100).toFixed(2);
            oportunity.payment.financy = +((value.financy * +oportunity.total)/100).toFixed(2);

            var entradaFormated = oportunity.payment.entry.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var semestraisFormated = oportunity.payment.semester.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var mensaisFormated = oportunity.payment.montly.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var financiamentoFormated = oportunity.payment.financy.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            var semestraisTotalFormated = oportunity.payment.semesterTotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
            var mensaisTotalFormated = oportunity.payment.montlyTotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

            $('#payment-entrada').html(entradaFormated);
            $('#get-slider-entrada').attr("value", +oportunity.payment.entry);
            $('#get-slider-entrada').attr("max", +oportunity.total);
            $('#get-percent-entrada-holder').css("width", ""+value.entry+"%");
            $('#get-percent-entrada').html(value.entry + "%");

            $('#payment-semestrais').html(semestraisTotalFormated);
            $('#semester-qts').html(semesterForDeliver + "x ");
            $('#get-slider-semestrais').attr("value", +value.semester);/* +oportunity.payment.semester */
            $('#get-slider-semestrais').attr("max", 100);/* +oportunity.total */
            $('#get-smaller-semestrais').html(semestraisFormated);
            $('#get-percent-semestrais-holder').css("width", ""+value.semester+"%");
            $('#get-percent-semestrais').html(value.semester + "%");

            $('#payment-mensais').html(mensaisTotalFormated);
            $('#months-qts').html(monthsForDeliver + "x ");
            $('#get-slider-mensais').attr("value", +value.montly);/* +oportunity.payment.montly */
            $('#get-slider-mensais').attr("max", 100);/* +oportunity.total */
            $('#get-smaller-mensais').html(mensaisFormated);
            $('#get-percent-mensais-holder').css("width", ""+value.montly+"%");
            $('#get-percent-mensais').html(value.montly + "%");

            $('#payment-financiamento').html(financiamentoFormated);
            $('#get-slider-financiamento').attr("value", +oportunity.payment.financy);
            $('#get-slider-financiamento').attr("max", +oportunity.total);
            $('#get-percent-financiamento-holder').css("width", ""+value.financy+"%");
            $('#get-percent-financiamento').html(value.financy + "%");

            //$('#get-slider-entrada')

            /* $('.payment-money').each(function( index ) {
                $( this ).html(valueFormated);
            }); */
            /* $('.get-slider').each(function( index ) {
                $( this ).attr("max", oportunity.total);
            }); */
            //$('#get-slider-entrada').attr("value", oportunity.total);
        }
        function diff_months(dt2, dt1) 
        {
            if(dt1 < dt2)
                return -1;

            var diff =(dt2.getTime() - dt1.getTime()) / 1000;
            diff /= (60 * 60 * 24 * 7 * 4);
            return Math.abs(Math.round(diff));
        }


    </script>
@endpush