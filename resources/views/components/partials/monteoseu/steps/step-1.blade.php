<div class="row">
    <div data-aos="fade-right" class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 1 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Escolha o empreendimento
            </h4>
            <p class="caption-15 text-preto-azulado-get mb-5 pb-4">
                Selecione abaixo o Get que mais lhe agrada:
            </p>

            @foreach($data as $key => $emp)
            @if(!empty($emp['floors']))
            <div data-aos="fade" class="builder-card  pb-3">
                <div class="d-flex">
                    <div class="custom-control check-emp-offset custom-radio get-select position-absolute">
                        <input
                            onclick="setEmpreendimento({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')"
                            style="box-shadow: none" type="checkbox" class="custom-control-input get-check-emp"
                            aria-describedby="movel-value-" id="emp-check-{{$emp['id']}}">
                        <label class="custom-control-label text-preto-azulado-get" for="emp-check-{{$emp['id']}}">

                        </label>
                    </div>
                    <p onclick="setEmpreendimento({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')"
                        class="click-link text-preto-azulado-get caption-16 mb-0 ml-4 pl-1">{{$emp['ch_title']}}</p>
                </div>
                <p onclick="setEmpreendimento({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')"
                    class="ml-4 pl-1 click-link caption-14 text-hex-797979 mb-3" style="max-width: 219px">
                    A partir de R$ {{ number_format($emp['price_unity'], 2, ',', '.') }}

                </p>
            </div>
            @endif
            @endforeach

        </div>
    </div>
    <div class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_emp_detail')" id="current-emp-select-area"
            class="item-detail-img mr-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-emp.png')}}');">
            <p id="current-emp-detail" class="caption-16 text-white item-detail-detail"></p>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_emp_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <img class="modal-item-img" id="modal_emp_detail-img" src="" alt="">

            </div>
        </div>
    </div>
</div>

@endpush