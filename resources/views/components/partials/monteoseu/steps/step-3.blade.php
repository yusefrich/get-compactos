<div class="row">{{-- unities --}}
    <div data-aos="fade-right" class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 3 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Escolha a planta
            </h4>
            <p style="max-width: 360px;" class="caption-15 text-preto-azulado-get mb-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu sagittis.
            </p>

            <div id="unities-select-area">
            </div>


        </div>
    </div>
    <div class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_unity_detail')" id="current-unity-select-area"
            class="item-detail-img mr-4"
            style="background-image:  url('{{url('assets_front/imgs/detail-bg-planta.png')}}');">
            <p class="caption-14 text-middle-gray item-detail-detail mb-0 value-total-title">Valor parcial</p>
            <h4  class="ml-32 text-middle-gray  value-total-class font-weight-bold"></h4>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt="">
        </div>
    </div>
</div>

@push('modals')
<div class="modal fade p-0" id="modal_unity_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog fullscreen-modal" role="document">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close btn btn-light btn-round  p-2 mr-2 mt-2" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">
                        <ion-icon name="close-outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <img class="modal-item-img" id="modal_unity_detail-img" src="" alt="">

            </div>
        </div>
    </div>
</div>
@endpush
@push('scripts')
<script>
    function selectUnity(unities, floor) {
        /* console.log(floor); */
        /* setItemMOS("#current-ap-select-area", "#modal_ap_detail-img", `{{url('storage/floor/${floor.img}')}}`); */
        $('#disabled-planta-selec').removeAttr("hidden");

        var uniHtml = '';
        console.log("selectUnity being called");
        console.log(unities);
        uniHtml = uniHtml+ '<div class="row">';
        unities.forEach(element => {
            uniHtml = uniHtml+
                '<div data-aos="fade" class="col-md-4 builder-card  pb-3">'+
                    `<div class="d-flex">`;
            if(element.sold == 0){
                uniHtml = uniHtml+ 
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input onclick='selectAp(${JSON.stringify(element)}, ${JSON.stringify(floor)}, ${JSON.stringify(element)})' style="box-shadow: none" type="radio"  name="plantaSelectRadio" data-value="${element.price}" class="custom-control-input get-check-emp select-unidade" aria-describedby="movel-value-" id="planta-check-${element.id}">`+
                            `<label class="no-wrap-text click-link custom-control-label text-preto-azulado-get" for="planta-check-${element.id}">`+
                                `<p class=" text-preto-azulado-get caption-16 mb-0">${element.name}</p>`+
                                `<p class="click-link caption-14 text-hex-797979 mb-0">+${element.price}</p>`+
                            `</label>`+
                        `</div>`;
            }else{
                uniHtml = uniHtml+ 
                        `<div  class="custom-control check-emp-offset custom-radio get-select">`+
                            `<input disabled onclick='selectAp(${JSON.stringify(element)}, ${JSON.stringify(floor)}, ${JSON.stringify(element)})' style="box-shadow: none" type="radio"  name="plantaSelectRadio"  class="custom-control-input get-check-emp" aria-describedby="movel-value-" id="planta-check-${element.id}">`+
                            `<label class="no-wrap-text click-link custom-control-label text-preto-azulado-get" for="planta-check-${element.id}">`+
                                `<p class=" text-preto-azulado-get caption-16 mb-0">${element.name}</p>`+
                                `<p style="color: #FF3000;" class="click-link caption-14 mb-0">VENDIDO</p>`+
                            `</label>`+
                        `</div>`;
            }

            uniHtml = uniHtml+ 
                    `</div>`+
                `</div>`;
        });
        uniHtml = uniHtml+'</div>';

        /* $('#disabled-uni-selec').removeAttr("disabled"); */
        $('#ap-select-area').html(uniHtml);
    }

</script>
@endpush