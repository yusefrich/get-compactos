<div class="row">
    <div data-aos="fade-right" class="col-md-5 text-center text-md-left mx-2 mx-md-0">
        <div class="ml-4 mt-32">
            <p class="caption-14 text-hex-797979">
                Passo 8 de 8
            </p>
            <h4 class="text-preto-azulado-get">
                Quero meu Get
            </h4>
            <p style="max-width: 360px;" class="caption-15 text-preto-azulado-get mb-5 pb-4">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eget felis ultrices enim nisi, sed. Vitae eu
                sagittis.
            </p>

            <div data-aos="fade-right" data-aos-delay="600" style="max-width: 555px;"
                class="mr-auto ml-auto px-4 px-md-0 d-none d-md-block d-print-none">
                {{-- desktop --}}
                {!! Form::open(['route' => 'contact.send', 'class' => 'text-start get-form-new']) !!}
                <div class="form-group ">
                    <label for="input-name" class="text-preto-azulado-get caption-14">Seu Nome</label>
                    {{-- <input type="text" class="form-control " id="input-name" name="name" placeholder=""> --}}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Seu Nome',
                    'id'=> 'input-name', 'required']) !!}
                </div>
                <div class="form-group  ">
                    <label for="input-email" class="text-preto-azulado-get caption-14">Email</label>
                    {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                    {!! Form::text('mail', null, ['class' => 'form-control','placeholder' =>
                    'seu@email.com', 'id'=> 'input-email', 'required']) !!}
                </div>
                <div class="form-group    ">
                    <label for="input-phone" class="text-preto-azulado-get caption-14">Telefone (com DDD)</label>
                    {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00)
                    00000-0000','placeholder' =>
                    '(00) 00000-0000', 'id'=>
                    'input-phone', 'required']) !!}
                </div>
                <div class="form-group    mx-0">
                    <label for="input-mensagem" class="text-preto-azulado-get caption-14">Mensagem (opcional)</label>
                    {!! Form::textarea('msg', null, ['class' => 'form-control ','rows' => 5,'id'=>
                    'input-mensagem', 'required']) !!}
                </div>
                <div class="form-group    mx-0">
                    {!! Form::hidden('emp_id', null, ['class' => 'form-control ', 'id'=>
                    'input-emp', 'required']) !!}
                </div>
                <div class="form-group    mx-0">
                    {!! Form::hidden('payment_sim', null, ['class' => 'form-control ', 'id'=>
                    'input-sim', 'required']) !!}
                </div>

                <div class="row pb-72 step-6-form">
                    <div class="col-5  pl-1">
                        <button class="btn btn-dark-get btn-block px-0" type="submit">Quero meu Get</strong></button>
                    </div>
                </div>
                </form>


            </div>
            <div data-aos="fade-right" data-aos-delay="600" style="max-width: 555px;"
                class="mr-auto ml-auto px-4 px-md-0 d-md-none d-print-none">
                {{-- mobile --}}
                {!! Form::open(['route' => 'contact.send', 'class' => 'text-start get-form']) !!}
                <div class="form-group ">
                    <label for="input-name-mob text-preto-azulado-get">Nome:</label>
                    {{-- <input type="text" class="form-control " id="input-name-mob" name="name" placeholder=""> --}}
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Seu Nome',
                    'id'=> 'input-name-mob', 'required']) !!}
                </div>
                <div class="form-group  ">
                    <label for="input-email-mob">Email:</label>
                    {{-- <input type="email" class="form-control" id="input-email"  name="email" placeholder=""> --}}
                    {!! Form::text('mail', null, ['class' => 'form-control','placeholder' =>
                    'seu@email.com', 'id'=> 'input-email-mob', 'required']) !!}
                </div>
                <div class="form-group    ">
                    <label for="input-phone-mob">Telefone:</label>
                    {!! Form::text('phone', null, ['class' => 'form-control ', 'data-mask' => '(00)
                    00000-0000','placeholder' =>
                    '(00) 00000-0000', 'id'=>
                    'input-phone-mob', 'required']) !!}
                </div>
                <div class="form-group    mx-0">
                    <label for="input-mensagem-mob">Mensagem:</label>
                    {!! Form::textarea('msg', null, ['class' => 'form-control ','id'=>
                    'input-mensagem-mob', 'required']) !!}
                </div>

                <div class="form-group    mx-0">
                    {!! Form::hidden('emp_id', null, ['class' => 'form-control ', 'id'=>
                    'input-emp-mob', 'required']) !!}
                </div>
                <div class="form-group    mx-0">
                    {!! Form::hidden('payment_sim', null, ['class' => 'form-control ', 'id'=>
                    'input-sim-mob', 'required']) !!}
                </div>
                <div class="row pb-72 step-6-form">
                    <div class="col-6 pr-1 ">
                        <a href="#pageTop" data-step-to="step-5" data-step-from="step-6" data-step-dir="prev"
                            class="btn btn-outline-dark btn-block btn-step-move px-0">Passo Anterior</strong></a>
                    </div>
                    <div class="col-6  pl-1">
                        <button class="btn btn-dark-get btn-block px-0" type="submit">Enviar proposta</strong></button>
                    </div>
                </div>
                </form>


            </div>

        </div>
    </div>
    <div class="col-md-7  item-detail-col">
        <div onclick="openModalItemDetail('#modal_resume_detail')" id="current-resume-select-area"
            class="item-detail-img mr-4">
            {{-- <p id="current-resume-detail" class="caption-16 text-white item-detail-detail"></p>
            <img class="item-zoom-icon" src="{{url('assets_front/icons-raw/item-icon-zoom.svg')}}" alt=""> --}}
            <div id="payment-op-print" class="d-none mx-5 pt-5">
                <div class="d-flex justify-content-between">
                    <div>
                        <p class="caption-14 text-middle-gray m-0">Valor total</p>
                        <h4 class="m-0 final-value-emp text-middle-gray mb-32">R$ 0,00</h4>
                    </div>

                    <button onclick="window.print();" class="btn mt-1 font-weight-bold d-print-none"> 
                        IMPRIMIR
                        {{-- <i class="icon icon-icon-print print-offset-icon pl-2"></i>  --}}
                        <img class="ml-2" src="{{url('assets_front/icons-raw/printer-2.svg')}}" alt="">
                    </button>

                </div>

                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p class="caption-14 text-middle-gray m-0 mb-1 mt-3 ">Empreendimento</p>
                        <div class="d-flex bg-white">
                            <p id="final-value-emp-name" class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold">Empreendimento </p>
                            <p id="final-value-uni-name" class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold">Unidade 1</p>
                        </div>
                    </div>
                    <p style="bottom: 0; right: 0;" id="final-value-uni-price" class="bg-white pl-2 position-absolute print-price-tag form-text caption-16 text-middle-gray mt-0 mb-0 final-value-plan">
                        6.532,56
                    </p>
                </div>
                <hr style="
                    margin: 0;
                    top: -9px;
                    position: relative;
                    background: #9C9C9C;
                ">
                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p class="caption-14 text-middle-gray m-0 mb-1 mt-3 ">Móveis Planejados</p>
                        <p  class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold final-value-movel-name bg-white">Kit 02 </p>
                    </div>
                    <p style="bottom: 0; right: 0;" class="bg-white pl-2 position-absolute print-price-tag form-text caption-16 text-middle-gray mt-0 mb-0 final-value-plan final-value-movel-price">
                        6.532,56
                    </p>
                </div>
                <hr style="
                    margin: 0;
                    top: -9px;
                    position: relative;
                    background: #9C9C9C;
                ">
                <div class="forni-list"></div>


                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p id="final-value-type" class="caption-14 text-middle-gray m-0 mb-1 mt-3 ">tipo 1</p>
                        <p id="final-value-typeDesc" class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold bg-white">desc </p>
                    </div>
                </div>

                <p class="caption-14 text-middle-gray m-0 mb-1 mt-3 ">Pagamento</p>
                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p  class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold bg-white">Entrada </p>
                    </div>
                    <p style="bottom: 0; right: 0;" id="final-value-entrada" class="bg-white pl-2 position-absolute print-price-tag form-text caption-16 text-middle-gray mt-0 mb-0 final-value-plan ">
                        6.532,56
                    </p>
                </div>
                <hr style="
                    margin: 0;
                    top: -9px;
                    position: relative;
                    background: #9C9C9C;
                ">
                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p  class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold bg-white">Mensais </p>
                    </div>
                    <div style="bottom: 0; right: 0;" class="d-flex position-absolute">
                        <p  id="final-value-mensais-qtd" class="bg-white form-text caption-14 text-middle-gray pl-2 mt-0 mb-0 final-value-plan position-relative">
                            6.532,56
                        </p>
                        <p id="final-value-mensais" class="bg-white pl-2  print-price-tag form-text caption-16 text-middle-gray mt-0 mb-0 final-value-plan ">
                            6.532,56
                        </p>
                    </div>
                </div>
                <hr style="
                    margin: 0;
                    top: -9px;
                    position: relative;
                    background: #9C9C9C;
                ">
                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p  class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold bg-white">Semestrais </p>
                    </div>
                    <div style="bottom: 0; right: 0;" class="d-flex position-absolute">
                        <p  id="final-value-semestrais-qtd" class="bg-white form-text caption-14 text-middle-gray pl-2 mt-0 mb-0 final-value-plan position-relative">
                            6.532,56
                        </p>
                        <p id="final-value-semestrais" class="bg-white pl-2  print-price-tag form-text caption-16 text-middle-gray mt-0 mb-0 final-value-plan ">
                            6.532,56
                        </p>
                    </div>
                </div>
                <hr style="
                    margin: 0;
                    top: -9px;
                    position: relative;
                    background: #9C9C9C;
                ">
                <div style="z-index: 1" class="d-flex position-relative">
                    <div class=" pr-2">
                        <p  class="caption-16 text-middle-gray m-0 mb-1 font-weight-bold bg-white">Financiamento </p>
                    </div>
                    <p style="bottom: 0; right: 0;" id="final-value-financiamento" class="bg-white pl-2 position-absolute print-price-tag form-text caption-16 text-middle-gray mt-0 mb-0 final-value-plan ">
                        6.532,56
                    </p>
                </div>
                <hr style="
                    margin: 0;
                    top: -9px;
                    position: relative;
                    background: #9C9C9C;
                ">


                {{-- <div id="payment-op-print" class="d-none">
                    <div style="margin-top: 16px" class="row">
                        <div class="col-6 mb-20">
                            <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Entrada</p>
                            <p id="final-value-entrada" class="m-0 caption-16 text-white ">R$ 14.000,00</p>
                        </div>
                        <div class="col-6 mb-20">
                            <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Semestrais</p>
                            <p id="final-value-semestrais" class="m-0 caption-16 text-white ">R$6.167</p>
                            <small id="final-value-semestrais-qtd" class="form-text text-white mt-0 final-value-plan">
                                6.532,56
                            </small>
                        </div>
                        <div class="col-6 mb-20">
                            <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Mensais</p>
                            <p id="final-value-mensais" class="m-0 caption-16 text-white ">R$ 1.200,00</p>
                            <small id="final-value-mensais-qtd" class="form-text text-white mt-0 final-value-plan">
                                6.532,56
                            </small>
                        </div>
                        <div class="col-6 mb-20">
                            <p class="caption-14 text-vermelho-get m-0 font-weight-bold">Financiamento</p>
                            <p id="final-value-financiamento" class="m-0 caption-16 text-white ">R$78,067,23</p>
                        </div>
                    </div>
                </div> --}}

            </div>

        </div>
    </div>
</div>