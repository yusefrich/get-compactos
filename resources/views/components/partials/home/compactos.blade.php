<div class="container-fluid background-cinza overflow-hidden position-relative">

    @php $feat = $feat->find(1); @endphp 
    @if($feat)
    <div class="row compactos">
        <div data-aos="fade-right"  class=" col-spacing-right col-md-5 d-flex justify-content-end">
            <img class="img-cover compactos-img d-none d-md-block" src="{{url('storage/home/'.$feat->img1)}}" alt="">
        </div>
        <div data-aos="fade-right" data-aos-delay="300" class="col-spacing-left col-md-7 ">
            <div class="d-flex justify-content-center justify-content-md-start">
                <img class="img-cover compactos-badge-img mb-20" src="{{url('storage/home/'.$feat->img2)}}" alt="">
            </div>
            <p class="text-dark paragraph-20  text-center text-md-left ml-auto mr-auto ml-md-0 mr-md-0" style="max-width: 455px;">{!! $feat->subtitle !!}</p>
        </div>
    </div>
    @endif

    {{-- 2 COMPATOS ROW --}}
    @php $feat = $feat->find(2); @endphp 
    @if($feat)
    <div class="row compactos-2 ">
        <div data-aos="fade-left"  class="col-spacing-right col-md-6 d-flex justify-content-end mobile-no-spacing">
            <img class="img-bg-pattern-right" src="{{url('assets_front/imgs/img-bg-pattern.svg')}}" alt="">
            <img class="img-cover ap1-img pl-md-40" src="{{url('storage/home/'.$feat->img1)}}" alt="">
        </div>
        <div data-aos="fade-left" data-aos-delay="300" class="col-spacing-left col-md-6">
            <h2 style="max-width: 485px;" class="text-preto-azulado-get compactos-2-text text-center text-md-left ml-auto mr-auto ml-md-0 mr-md-0 mb-0">
                {{$feat->title}}
            </h2>
            <p style="max-width: 483px;" class="text-dark paragraph-20  text-center text-md-left ml-auto mr-auto ml-md-0 mr-md-0 mob-mb-60" style="max-width: 490px;">{!! $feat->subtitle !!}</p>
        </div>
    </div>
    @endif

    {{-- 3 COMPACTOS ROW --}}
    @php $feat = $feat->find(3); @endphp 
    @if($feat)
    {{-- DESKTOP --}}
    <div class="d-none d-md-block">
        <div class="row compactos-3 pb-100">
            <div data-aos="fade-right"  class="col-spacing-right col-md-6 d-flex justify-content-end pb-120">
                <div>
                    <h2 style="max-width: 485px;" class="text-preto-azulado-get mb-0">
                        {{$feat->title}}
                    </h2>
                    <p class="text-dark paragraph-20 " style="max-width: 483px;">{!! $feat->subtitle !!}</p>
                </div>
            </div>
            <div data-aos="fade-up"  data-aos-delay="300" class="col-spacing-left col-md-6 ">
                <img class="img-bg-pattern-left" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">

                <img data-aos="fade-right" class="img-cover ap2-img pr-md-40" src="{{url('storage/home/'.$feat->img1)}}"   alt="">
                <img data-aos="fade-right" class="img-cover ap3-img" src="{{url('storage/home/'.$feat->img2)}}" alt="">
            </div>
        </div>
    </div>
    {{-- MOBILE --}}
    <div class="d-md-none">
        <div data-aos="fade-up"  data-aos-delay="300" style="margin-bottom: 140px" class="mobile-no-spacing d-flex justify-content-end position-relative">
            <img class="img-bg-pattern-left" src="{{url('assets_front/imgs/img-bg-pattern.png')}}" alt="">
            <img data-aos="fade-right" class="img-cover ap2-img-mobile " src="{{url('storage/home/'.$feat->img1)}}" alt="">
            <img data-aos="fade-right" class="img-cover ap3-img-mobile" src="{{url('storage/home/'.$feat->img2)}}" alt="">
        </div>

        <div data-aos="fade-right"  class=" d-flex justify-content-center">
            <div>
                <h2 style="max-width: 485px;" class="text-preto-azulado-get text-center">
                    {{$feat->title}}
                </h2>
                <p class="text-dark paragraph-20 text-spacing text-center" style="max-width: 483px;">{!! $feat->subtitle !!}</p>
            </div>
        </div>

    </div>

    @endif
    <div  data-aos="fade-bottom" class="d-flex justify-content-center compactos-btn btn-more">
        <a style="z-index: 10" href="{{ route('about') }}" class="btn btn-outline-dark">Sobre a Get</a>
    </div>
    <img class="hex-bg-pattern d-none d-md-block" src="{{url('assets_front/imgs/hex-bg-pattern.png')}}" alt="">
</div>
