<div id="background-slider" style=" background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            width: 100%;" class="">
    {{-- background:linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url('{{url('assets_front/imgs/banner-initial-bg.png')}}');
    --}}
    <div id="background-slider-transition" style="background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            width: 100%;"></div>
    <div class="bg-banner-black-full"></div>

    <div class=" d-none d-md-block v-center container-xl mr-auto ml-auto ">
        <div data-aos="fade-right" class="social-banner text-center">
            <p>083 3031-9191</p>
            <a style="padding: 6px"class="btn btn-light btn-round m-8" target="_blank" href="tel:83-3031-9191">
                {{-- <ion-icon style="font-size: 24px;" name="call"></ion-icon> --}}
                <img src="{{asset('assets_front/imgs/phone-icon.svg')}}" alt="phone icon">
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos.">
                <ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon>
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://www.instagram.com/getcompactos/">
                <ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon>
            </a>
        </div>
        <div id="carouselBannerHome" class="carousel  " data-ride="carousel">
            <div class="row m-0">
                <div class="col-6"></div>
                <div data-aos="fade-up" data-aos-delay="900" class="col-6">
                    <div class="d-flex indicators-holder">
                        <div class="indicators-counter">
                            <p class="text-white m-0"><span class="font-weight-bold current-carousel-pos">1/</span>
                                <span class="font-weight-medium">{{count($home)}}</span>
                            </p>
                        </div>
                        <ol id="indicators-banner-carousel" style="margin-top: 15px" class="carousel-indicators">
                            @foreach($home as $key => $h)
                            <li data-target="#carouselBannerHome" data-slide-to="{{$key}}"
                                class="d-flex justify-content-center  @if($key == 0) active @endif">
                                <ion-icon style="font-size: 5px; margin-top: 4.3px; margin-left: .5px" class="text-white" name="ellipse">
                                </ion-icon>
                            </li>
                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
            <div data-aos="fade-right" data-aos-delay="300" class="carousel-inner"> {{--  --}}
                @foreach($home as $key => $h)
                <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                    <div class="row m-0 ">
                        <div class="col-6 text-right banner-img-holder anim-intro ">
                            <img class="img-cover img-cover-size" src="{{url('storage/feat/'.$h->img2)}}" 
                                alt="">
                            <img class="img-banner-badge" src="{{url('storage/feat/'.$h->img3)}}" alt="">
                        </div>
                        <div class="col-6 ">
                            <div  class="v-center banner-initial-ml">
                                <h1 class="banner-text-margin anim-intro " style="max-width: 541px; animation-delay: .3s;">{{$h->title}}</h1>
                                <p style="max-width: 504px; animation-delay: .6s;" class="text-white anim-intro">{{$h->subtitle}}</p>
                                <a href="{{$h->bt_url}}" style="animation-delay: .9s; margin-top: 16px" class="btn btn-outline-primary anim-intro">
                                    {{$h->bt_title}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>


    </div>

    <div class="d-md-none container-fluid mobile-banner">
        <div id="carouselBannerHomeMobile" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner pt-100">
                @foreach($home as $key => $h)
                <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                    <div  class=" text-center ">
                        <h1 class="banner-text-margin mx-auto" style="max-width: 314px;">{{$h->title}}</h1>
                        <p style="max-width: 302px;" class="text-white mx-auto mb-20">
                            {{-- {{$h->subtitle}} --}}
                            {!! \Illuminate\Support\Str::limit($h->subtitle, 80, '...')!!}
                        </p>
                        {{-- <button class="btn btn-outline-primary">
                            {{$h->bt_title}}
                        </button> --}}
                    </div>
                    <div class=" text-center ">
                        <img class="img-cover mobile-img-banner mx-auto" src="{{url('storage/feat/'.$h->img2)}}" 
                            alt="">
                        <img class="img-banner-badge" src="{{url('storage/feat/'.$h->img3)}}" alt="">
                    </div>

                </div>
                @endforeach
            </div>
            <div class="d-flex indicators-holder-mobile justify-content-center mt-20">
                <div class="indicators-counter">
                    <p class="text-white m-0"><span class="font-weight-bold current-carousel-pos">1/</span>
                        <span class="font-weight-medium">{{count($home)}}</span>
                    </p>
                </div>
                <ol id="indicators-carouselBannerHomeMobile" class="carousel-indicators mt-3">
                    @foreach($home as $key => $h)
                    <li data-target="#carouselBannerHomeMobile" data-slide-to="{{$key}}"
                        class="d-flex justify-content-center  @if($key == 0) active @endif">
                        <ion-icon style="font-size: 5px; margin-top: 4px" class="text-white" name="ellipse">
                        </ion-icon>
                    </li>
                    @endforeach
                </ol>
            </div>
            <div class="d-flex justify-content-center">
                <div class="mt-40 mb-50">
                    <p  class="text-white mb-0 ml-2">|</p>
                    <ion-icon style="font-size: 22px" class="text-white" name="chevron-down-outline"></ion-icon>
                </div>
            </div>
    
        </div>

        {{-- <h1 style="max-width: 314px;" class="text-white text-center pt-100 mr-auto ml-auto">Uma nova maneira de ser e viver</h1>
        <p style="max-width: 302px;" class="text-white text-center mob-paragraph-16 mr-auto ml-auto">
            If you are tired of living in your home as it is, or it simply is not big enough for you and your family anymore
        </p> --}}
    </div>

</div>

@push('scripts')
<script>
    /* indicators-banner-carousel */
        $( document ).ready(function() {
            var home = {!! json_encode($home->toArray()) !!};
            var url = '{!! url('storage/feat/') !!}';

            console.log(url);
            console.log(home);
            $("#background-slider").css("background-image", "linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url("+ url + "/" + home[0].img1 + ")"); 
            $("#background-slider-transition").css("background-image", "linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url("+ url + "/" + home[0].img1 + ")"); 

            // carousel bg transition of desktop
            $('#carouselBannerHome').on('slide.bs.carousel', function (carousel) {
                $("#background-slider-transition").css("background-image", "linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url("+ url + "/" + home[carousel.to].img1 + ")"); 
                $("#background-slider-transition").css("opacity", "1");
                /* anim-intro */

                console.log(carousel);
                $(".anim-intro").removeClass("fadeInRight");
                $(this).find(".anim-intro").addClass("animated fadeInRight");

                // do something...  
                /* console.log(carousel.to); */
                $("#carouselBannerHome .current-carousel-pos" ).html((carousel.to + 1) + "/" ); 
            })
            $('#carouselBannerHome').on('slid.bs.carousel', function (carousel) {
                $("#background-slider").css("background-image", "linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url("+ url + "/" + home[carousel.to].img1 + ")"); 
                $("#background-slider-transition").css("opacity", "0");

            })
            // carousel bg transition of mobile
            $('#carouselBannerHomeMobile').on('slide.bs.carousel', function (carousel) {
                $("#background-slider-transition").css("background-image", "linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url("+ url + "/" + home[carousel.to].img1 + ")"); 
                $("#background-slider-transition").css("opacity", "1");

                // do something...  
                /* console.log(carousel.to); */
                $("#carouselBannerHomeMobile .current-carousel-pos" ).html((carousel.to + 1) + "/" ); 
            })
            $('#carouselBannerHomeMobile').on('slid.bs.carousel', function (carousel) {
                $("#background-slider").css("background-image", "linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url("+ url + "/" + home[carousel.to].img1 + ")"); 
                $("#background-slider-transition").css("opacity", "0");

            })
        });
</script>
@endpush
