{{-- desktop --}}
<div class="d-none d-md-block container-fluid p-0 background-dark overflow-hidden">
    <div class="d-flex justify-content-start">

        {{-- <iframe style="max-width: 541px"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15837.012534506515!2d-34.84452416347689!3d-7.096631267999787!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5641e069f630deb!2sEdif%C3%ADcio%20Tropical%20Varandas%20Residence!5e0!3m2!1spt-BR!2sbr!4v1585695580539!5m2!1spt-BR!2sbr"
            width="100%" height="484" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
            tabindex="0"></iframe> --}}
        <div style="width: 541px; height: 484px;" id="map"></div>
        <div data-aos="fade-right" class="mapa-text-spacing">
            <h3 style="max-width: 356px;" class="text-white">Ficou com dúvidas? Let’s Get a coffee!</h3>
            <p style="max-width: 456px;" class="text-white">Para saber mais sobre os nossos empreendimentos, envie sua mensagem e em breve entraremos em contato com você!</p>
            <a class="btn btn-outline-primary" href="{{ route('contact') }}">Marque um café</a>
        </div>
    </div>
</div>
{{-- mobile --}}
<div class="d-md-none container-fluid p-0 background-dark overflow-hidden">
    <div class="d-flex justify-content-center my-42">
        <div data-aos="fade-right" class="text-center">
            <h3 style="max-width: 356px;" class="text-white mx-auto ">Ficou com dúvidas? Let’s Get a coffee!</h3>
            <p style="max-width: 456px;" class="text-white mx-auto">Para saber mais sobre os nossos empreendimentos, envie sua mensagem e em breve entraremos em contato com você!</p>
            <a class="btn btn-outline-primary" href="{{ route('contact') }}">Marque um café</a>
        </div>
    </div>
    <div style="width: 100%; height: 221px;" id="mapMobile"></div>

</div>

{{-- script de edição de mapa --}}
@push('scripts')
    
<script>
    function initMap() {
        var mapStyles = [
            {elementType: 'geometry', stylers: [{color: '#333333'}]}, /* 242f3e */
            {elementType: 'labels.text.stroke', stylers: [{color: '#333333'}]}, /* 333333 */
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            
            {
                featureType: 'administrative.locality',
                elementType: 'labels.text.fill',
                stylers: [{color: '#eeeeee'}]
            },
            /* {
                featureType: "all",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            }, */
            {
                featureType: 'poi',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}, { visibility: "off" }]
            },
            {
                featureType: 'poi.park',
                elementType: 'geometry',
                stylers: [{color: '#263c3f'}]
            },
            {
                featureType: 'poi.park',
                elementType: 'labels.text.fill',
                stylers: [{color: '#ffffff'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [{color: '#282828'}]
            },
            {
                featureType: 'road',
                elementType: 'geometry.stroke',
                stylers: [{color: '#333333'}]
            },
            {
                featureType: 'road',
                elementType: 'labels.text.fill',
                stylers: [{color: '#9ca5b3'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry',
                stylers: [{color: '#746855'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'geometry.stroke',
                stylers: [{color: '#1f2835'}]
            },
            {
                featureType: 'road.highway',
                elementType: 'labels.text.fill',
                stylers: [{color: '#f3d19c'}]
            },
            {
                featureType: 'transit',
                elementType: 'geometry',
                stylers: [{color: '#2f3948'}]
            },
            {
                featureType: 'transit.station',
                elementType: 'labels.text.fill',
                stylers: [{color: '#eeeeee'}]
            },
            {
                featureType: 'water',
                elementType: 'geometry',
                stylers: [{color: '#2B2B2B'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.fill',
                stylers: [{color: '#515c6d'}]
            },
            {
                featureType: 'water',
                elementType: 'labels.text.stroke',
                stylers: [{color: '#2B2B2B'}]
            }
        ];
      // Styles a map in night mode.
        console.log("map being init");
        var mapEl = document.getElementById('map');
        var mapElMob = document.getElementById('mapMobile');
        var map;
        if(mapEl){
            map = new google.maps.Map(mapEl, {
                center: {lat: -7.094271, lng: -34.836458},
                zoom: 15,
                disableDefaultUI: true,
                styles: mapStyles
            });
            var marker = new google.maps.Marker({position: {lat: -7.094271, lng: -34.836458}, title: "Get", map: map, icon: '{!! url('assets_front/imgs/marker.png') !!}'});
        }
        if(mapElMob){
            map = new google.maps.Map(mapElMob, {
                center: {lat: -7.094271, lng: -34.836458},
                zoom: 15,
                disableDefaultUI: true,
                styles: mapStyles
            });
            var marker = new google.maps.Marker({position: {lat: -7.094271, lng: -34.836458}, title: "Get", map: map, icon: '{!! url('assets_front/imgs/marker.png') !!}'});
        }




    }
</script>


<script src="https://maps.googleapis.com/maps/api/js?key={{config('app.map_key')}}&callback=initMap"
async defer></script>

@endpush
