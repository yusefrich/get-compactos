<div class="container-fluid px-0 px-md-3  bg-empreendimentos empreendimentos-block position-relative overflow-hidden">
    <div class="row empreendimentos emp-spacing-bottom">
        <div data-aos="fade-right" class="col-md-7 d-flex justify-content-center justify-content-md-end">
            <h1 class="title-72 text-white">Empreendimentos</h1>
        </div>
        <div data-aos="fade-left" class="col-md-5">
            <p class="text-white paragraph-20 text-center text-md-left mx-auto mx-md-0" style="max-width: 391px;">
                Cenários urbanos transformados por meio da arquitetura, engenharia, design, tecnologia, amor e criatividade. Qual é o seu Get?
            </p>
        </div>
    </div>
    <img data-aos="fade-down" class="map-bg-pattern" src="{{url('assets_front/imgs/empreendimento-bg-pattern.png')}}"
        alt="">

    <div data-aos="fade-up" id="carouselEmpreendimentosCall" class="carousel slide pb-100 "
        data-ride="carousel">
        {{-- <ol class="carousel-indicators">
            <li data-target="#carousel-empreendimentos" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-empreendimentos" data-slide-to="1"></li>
        </ol> --}}

        <div data-aos="fade-left" data-aos-delay="600" class=" row m-0 px-26">
            <div class="col-6 d-flex justify-content-end">
                <div style="width: 100%; max-width: 485px;" class=" d-flex indicators-holder pl-3 pl-md-0 t-0">
                    <div class="indicators-counter">
                        <p class="text-white m-0"><span class="font-weight-bold">1/</span><span
                                class="font-weight-medium">{{count($emp)}}</span></p>
                    </div>
                    <ol class="carousel-indicators pt-2">
                        @foreach($emp as $key => $e)
                        <li data-target="#carouselEmpreendimentosCall" data-slide-to="{{$key}}" class="d-flex justify-content-center @if($key==0) active @endif">
                            <ion-icon style="font-size: 5px; margin-top: 4.3px; margin-left: .4px" class="text-white" name="ellipse">
                            </ion-icon>
                        </li>
                        @endforeach
                    </ol>
                </div>
            </div>
        </div>
        <div data-aos="fade-right" data-aos-delay="300" class="carousel-inner pb-4">
            @foreach($emp as $key => $e)
            <div class="carousel-item  @if($key == 0) active @endif">
                {{-- DESKTOP --}}
                <div class="d-none d-md-block">
                    <div class="row">
                        <div class=" col-md-6 emp-spacing-right d-flex justify-content-end ">
                            <div class="mt-70 mb-20"  style="min-height: 600px;">
                                <a href="{{route('emp', $e->slug)}}"><h3 class="text-white" style="max-width: 435px">{{$e->title}}</h3></a>
                                <a  href="{{route('emp', $e->slug)}}">
                                    <p class="text-white" style="max-width: 459px">
                                        {!! \Illuminate\Support\Str::limit($e->txt, 135, '...')!!}
                                    </p>
                                </a>
                                <div class="row empreendimentos-icons-spacing position-absolute ">
                                    @if(($e->txt1 !== null) && ($e->txt1 !== ""))   
                                    <div class="col-4">
                                        <a href="{{route('emp', $e->slug)}}">        
                                            <i class="text-white icon icon-caracteristicas {{$e->icon1}}"></i>
                                            <p class="caption-16 text-white">{{$e->txt1}}</p>
                                        </a>     
                                    </div>
                                    @endif
                                    @if(($e->txt2 !== null) && ($e->txt2 !== ""))                
                                    <div class="col-4">
                                        <a href="{{route('emp', $e->slug)}}">
                                            <i class="text-white icon icon-caracteristicas {{$e->icon2}}"></i>
                                            <p class="caption-16 text-white">{{$e->txt2}}</p>
                                        </a>
                                    </div>
                                    @endif
                                    @if(($e->txt3 !== null) && ($e->txt3 !== ""))                
                                    <div class="col-4">
                                        <a href="{{route('emp', $e->slug)}}">
                                            <i class="text-white icon icon-caracteristicas {{$e->icon3}}"></i>
                                            <p class="caption-16 text-white">{{$e->txt3}}</p>
                                        </a>
                                    </div>
                                    @endif
                                    @if(($e->txt4 !== null) && ($e->txt4 !== ""))                
                                    <div class="col-4">
                                        <a href="{{route('emp', $e->slug)}}">
                                            <i class="text-white icon icon-caracteristicas {{$e->icon4}}"></i>
                                            <p class="caption-16 text-white">{{$e->txt4}}</p>
                                        </a>
                                    </div>
                                    @endif
                                    @if(($e->txt5 !== null) && ($e->txt5 !== ""))                
                                    <div class="col-4">
                                        <a href="{{route('emp', $e->slug)}}">
                                            <i class="text-white icon icon-caracteristicas {{$e->icon5}}"></i>
                                            <p class="caption-16 text-white">{{$e->txt5}}</p>
                                        </a>
                                    </div>
                                    @endif
                                    @if(($e->txt6 !== null) && ($e->txt6 !== ""))                
                                    <div class="col-4">
                                        <a href="{{route('emp', $e->slug)}}">
                                            <i class="text-white icon icon-caracteristicas {{$e->icon6}}"></i>
                                            <p class="caption-16 text-white">{{$e->txt6}}</p>
                                        </a>
                                    </div>
                                    @endif
                                    <a href="{{route('emp', $e->slug)}}"
                                        class="btn btn-outline-primary mt-40">Conhecer Empreendimento</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 emp-spacing-left">
                            <img onclick="location.href= '{{ route('emp', $e->slug)}}' ;" class="img-cover apemp-img click-link" src="{{url('storage/emp/'.$e->img1)}}" alt="">
                        </div>
                    </div>
                </div>
                {{-- MOBILE --}}
                <div class=" d-md-none">
                    <div class="row mx-0 pt-72">
                        <div class=" col-md-6 emp-spacing-right d-flex justify-content-start ">
                            <div class="px-26">
                                <h3 style="max-width: 435px">{{$e->title}}</h3>
                                <p class="text-white" style="max-width: 459px">
                                    {!! \Illuminate\Support\Str::limit($e->txt, 50, '...')!!}
                                </p>

                            </div>
                        </div>
                        <div class="col-md-6 p-0 ">
                            <img class="img-cover apemp-img" src="{{url('storage/emp/'.$e->img1)}}" alt="">
                        </div>

                        <div class=" col-md-6 mx-26 px-0 d-flex justify-content-start ">
                            <div class=" "  style="min-height: 450px;">
                                <div class="row empreendimentos-icons-spacing position-absolute mx-0">
                                    <div class="col-4">
                                        <i class="text-white icon icon-caracteristicas {{$e->icon1}}"></i>
                                        {{-- <img class="icon-emp" src="{{url('storage/emp/'.$e->icon1)}}" alt=""> --}}
                                        <p class="caption-16 text-white">{{$e->txt1}}</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="text-white icon icon-caracteristicas {{$e->icon2}}"></i>
                                        {{-- <img class="icon-emp" src="{{url('storage/emp/'.$e->icon2)}}" alt=""> --}}
                                        <p class="caption-16 text-white">{{$e->txt2}}</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="text-white icon icon-caracteristicas {{$e->icon3}}"></i>
                                        {{-- <img class="icon-emp" src="{{url('storage/emp/'.$e->icon3)}}" alt=""> --}}
                                        <p class="caption-16 text-white">{{$e->txt3}}</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="text-white icon icon-caracteristicas {{$e->icon4}}"></i>
                                        {{-- <img class="icon-emp" src="{{url('storage/emp/'.$e->icon4)}}" alt=""> --}}
                                        <p class="caption-16 text-white">{{$e->txt4}}</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="text-white icon icon-caracteristicas {{$e->icon5}}"></i>
                                        {{-- <img class="icon-emp" src="{{url('storage/emp/'.$e->icon5)}}" alt=""> --}}
                                        <p class="caption-16 text-white">{{$e->txt5}}</p>
                                    </div>
                                    <div class="col-4">
                                        <i class="text-white icon icon-caracteristicas {{$e->icon6}}"></i>
                                        {{-- <img class="icon-emp" src="{{url('storage/emp/'.$e->icon6)}}" alt=""> --}}
                                        <p class="caption-16 text-white">{{$e->txt6}}</p>
                                    </div>
                                        <a href="{{route('emp', $e->slug)}}"
                                            class="btn btn-outline-primary px-0 btn-block mt-40">Conhecer Empreendimento</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    </div>
</div>
