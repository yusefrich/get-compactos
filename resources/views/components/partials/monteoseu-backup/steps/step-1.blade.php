<div class="row">
    <div data-aos="fade-right"  class="col-md-4 text-center text-md-left mx-2 mx-md-0">
        <img src="{{url('assets_front/imgs/building-icon.png')}}" class="pb-2" alt="">
        <h4 class="text-preto-azulado-get">
            Escolha o empreendimento
        </h4>
        <p class="mob-paragraph-16 text-preto-azulado-get">
            Selecione o Get dos seus sonhos e siga para as próximas etapas
        </p>
    </div>
    <div class="col-md-8 pb-40">
        @foreach($data as $key => $emp)
        @if(!empty($emp['floors']))
        <div data-aos="fade" class="builder-card mb-50 mb-mob-30 pb-3">
            <div class="row mx-0">
                <div class="col-6 col-md-4 pr-2 pr-11 position-relative">
                    <img onclick="setEmpreendimentoAndScroll({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')" class="click-link img-cover img-small-builder builder-item-img img-hover-builder" src="{{url('storage/emp/'.$emp['ch_img'])}}" alt="">
                    
                    <div id="check-emp-selected-bg-bg-{{$emp['id']}}" class="emp-bg-selected emp-bg-bg">
                        <div class="bg-selected img-small-builder"></div>
                    </div>
                    <div id="check-emp-selected-{{$emp['id']}}" class=" op-0 pr-11 emp-bg-selected emp-bg-check">
                        <div class="bg-selected img-small-builder"></div>
                        <img src="{{url('assets_front/imgs/check.svg')}}" class="pb-2 check-icon-emp " alt="">
                    </div>
                </div>
                <div class="col-6 col-md-8 pl-0">
                    <div class="d-flex">
                        <div  class="custom-control check-emp-offset custom-checkbox get-checkbox">
                            <input onclick="setEmpreendimentoAndScroll({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')"
                            style="box-shadow: none" 
                            type="checkbox" 
                            class="custom-control-input get-check-emp" 
                            aria-describedby="movel-value-" 
                            id="emp-check-{{$emp['id']}}">
                            <label class="custom-control-label text-preto-azulado-get" 
                            for="emp-check-{{$emp['id']}}">
                                
                            </label>
                        </div>    
                        <h4 onclick="setEmpreendimentoAndScroll({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')" class="click-link text-preto-azulado-get h4-mob">{{$emp['ch_title']}}</h4>
                    </div>
                    <p onclick="setEmpreendimentoAndScroll({{$emp['id']}}, {{json_encode($emp)}}, '#emp-check-{{$emp['id']}}')" class="click-link mob-paragraph-16 text-preto-azulado-get mb-3" style="max-width: 219px">
                        {{$emp['ch_subtitle']}}
                    </p>
                    <button onclick="setEmpreendimentoAndScroll({{$emp['id']}}, {{json_encode($emp)}}, 'emp-check-{{$emp['id']}}' )" id="btn-builder-{{$emp['id']}}" class="btn btn-outline-dark btn-smaller">
                        {{$emp['ch_title']}}
                    </button>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>

