<div class="container-small ml-auto mr-auto post-offset-top">
    <a data-aos="fade-up" data-aos-delay="400" href="{{ route('conteudos') }}" class="btn btn-transparent d-none d-md-inline-block" style="box-shadow: none">
        <ion-icon size="small" name="chevron-back-sharp"></ion-icon>
        <ion-icon size="small" class="dash-icon-btn" name="remove-sharp"></ion-icon> Voltar
    </a>
    <div style="margin-top: 45px; " class="row mx-0 title-spacing">
        <div data-aos="fade-right" id="socials" class="col-2 socials-display    ">
            <a style="padding: 8px 10px;" class="btn btn-gray btn-round  m-8" href="#"><i
                    class="fab fa-15x fa-whatsapp"></i></a>
            <a style="padding: 8px 14px;" class="btn btn-gray btn-round  m-8" href="#"><i
                    class="fab fa-15x fa-facebook-f"></i></a>
            <a style="padding: 9px 9px;" class="btn btn-gray btn-round  m-8" href="#"><i
                    class="fab fa-15x fa-facebook-messenger"></i></a>
            <a style="padding: 8px 11px;" class="btn btn-gray btn-round  m-8" href="#"><i
                    class="fab fa-15x fa-linkedin-in"></i></a>

        </div>
        <div data-aos="fade-left" data-aos-delay="200" class="col-md-10 ">
            <div class="post-title ">
                <span class="span-16 text-dark">{{$data->date}}</span>
                <br class="d-md-none">
                <span class="caption-18-600 text-dark">Por: <strong> {{$data->name}}</strong></span>
                <h1 style="max-width: 522px;" class="text-dark mt-3">
                    {{$data->title}}
                </h1>
                <p style="line-height: 34px; max-width: 555px;">
                    {{$data->subtitle}}
                </p>
            </div>
        </div>
    </div>
</div>
<div data-aos="fade-right" id="post-content" class="content">
    {!! $data->txt !!}
</div>
<div class="d-md-none text-center mobile-socials">
    <p class="text-uppercase mb-0">Compartilhe</p>
    <div data-aos="fade-right" id="socials-mobile" class="">
        <a style="padding: 8px 9px;" class="btn btn-gray btn-round  m-8 " href="#"><i
                class="fab fa-15x fa-whatsapp"></i></a>
        <a style="padding: 8px 11px;" class="btn btn-gray btn-round  m-8" href="#"><i
                class="fab fa-15x fa-facebook-f"></i></a>
        <a style="padding: 9px 9px;" class="btn btn-gray btn-round  m-8" href="#"><i
                class="fab fa-15x fa-facebook-messenger"></i></a>
        <a style="padding: 8px 9px;" class="btn btn-gray btn-round  m-8" href="#"><i
                class="fab fa-15x fa-linkedin-in"></i></a>

    </div>

    <a data-aos="fade-up" data-aos-delay="400" href="{{ route('conteudos') }}" class="btn btn-transparent socials-back-smaller mt-4" style="box-shadow: none">
        <ion-icon size="small" style="top: 2px" class="position-relative" name="chevron-back-sharp"></ion-icon>
        <ion-icon size="small" style="top: 2px"class=" dash-icon-btn" name="remove-sharp"></ion-icon> Voltar
    </a>

</div>
@push('scripts')
    <!-- navbar top opacity -->
<script>
    $(document).ready(function () {

        var setNavbarOpacity = function () {
            var o1 = $("#menu").offset();
            var o2 = $("body").offset();
            var dx = o1.left - o2.left;
            var dy = o1.top - o2.top;
            var distance = Math.sqrt(dx * dx + dy * dy);

            if (distance > 200) {
                $("#socials").addClass("fixed-socials animated fadeIn");
            } else {
                $("#socials").removeClass("fixed-socials fadeIn");
                $("#socials").addClass("fadeIn");
                
            }
        }
        var getPostDistance = function () {
            var o1 = $("#post-content").offset();
            var o2 = $("#socials").offset();
            var dx = o1.left - o2.left;
            var dy = o1.top - o2.top;
            var distance = Math.sqrt(dx * dx + dy * dy);
            console.log("objts");
            var postPos = o1.top + $("#post-content").height();
            var socPos = o2.top + $("#socials").height();

            if (postPos < socPos) {
                $("#socials").removeClass("fixed-socials fadeIn");
                $("#socials").addClass("fadeIn");

            } else if(distance < 200){
                $("#socials").addClass("fixed-socials animated fadeIn");
            }
        }

        setNavbarOpacity();

        $(window).scroll(function (event) {
            setNavbarOpacity();
            getPostDistance();

        });
    });
</script>


@endpush
