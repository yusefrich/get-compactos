<div class="container-fluid background-cinza conteudos-list" id="list">
    <div class="d-none d-md-block">
        <div class="container-smaller ml-auto mr-auto d-flex justify-content-between pt-20">
            <div class="list-btns">
                <a href="?q=all#list"
                    class="btn btn-dark text-uppercase 
                    @if($q == 'all') active @endif">
                    TODOS
                </a>
                <a href="?q=recent#list"
                    class="btn btn-dark text-uppercase 
                    @if($q == 'recent') active @endif">
                    Mais recentes
                </a>
                <a href="?q=old#list"
                    class="btn btn-dark text-uppercase 
                    @if($q == 'old') active @endif">
                    MAis antigos
                </a>
            </div>
            {!! Form::open(['method' => 'GET', 'class' => 'd-flex justify-content-end mb-0']) !!}
            <ion-icon class="pt-1" style="font-size: 30px" name="search-outline"></ion-icon>
                {!! Form::text('q', null, ['class' => 'form-control', 'placeholder' => 'BUSCAR',
                'id'=> 'input-search']) !!}
            {{ Form::close() }}
        </div>
    </div>
    <div class="d-md-none">
        <div class="container-smaller ml-auto mr-auto d-flex justify-content-between pt-20">
            <div class="list-btns d-flex overflow-auto">
                <a href="?q=all#list"
                    class="btn btn-dark text-uppercase 
                    @if($q == 'all') active @endif">
                    TODOS
                </a>
                <a href="?q=recent#list"
                    class="btn btn-dark text-uppercase 
                    @if($q == 'recent') active @endif">
                    Mais recentes
                </a>
                <a href="?q=old#list"
                    class="btn btn-dark text-uppercase 
                    @if($q == 'old') active @endif">
                    MAis antigos
                </a>
                {!! Form::open(['method' => 'GET', 'class' => 'd-flex justify-content-end mb-0', 'style' => 'min-width: 170px;']) !!}
                <ion-icon class="pt-1" style="font-size: 30px" name="search-outline"></ion-icon>
                    {!! Form::text('q', null, ['class' => 'form-control', 'placeholder' => 'BUSCAR',
                    'id'=> 'input-search']) !!}
                {{ Form::close() }}
            </div>
        </div>
    </div>

    <hr>
    <div class="container-smaller ml-auto mr-auto">
        <div class="row blog-destaques pt-40">
            @forelse($blog as $b)
            <div class="col-md-4">
                <div class="card card-transparent card-width" style=""> {{--  --}}
                    <img onclick="location.href= '{{ route('conteudo', $b->slug)}}';" src="{{url('storage/blog/'.$b->img)}}" class="img-cover card-img-top click-link" alt="...">
                    <div class="card-body px-0 text-center text-md-left">
                        <span class="span-16">{{$b->date}}</span>
                        <a href="{{ route('conteudo', $b->slug)}}">
                            <h4 class="text-preto-azulado-get">
                                {!! \Illuminate\Support\Str::limit($b->title, 40, '...')!!}
                            </h4>
                        </a>
                        <a href="{{ route('conteudo', $b->slug)}}" class="btn btn-outline-dark blog-destaques-btn-spacing btn-smaller">Ler mais</a>
                    </div>
                </div>
            </div>
            @empty
            <div class="col-md-12 text-center pb-100 pt-100">
                <p> <span>Nenhum conteúdo foi encontrado com o termo "{{$q}}" </span> </p>
            </div>
            @endforelse
        </div>

        <div class="d-flex justify-content-center btn-blog-caller-spacing">
            <button class="btn btn-outline-dark">Ver Conteúdos</button>
        </div>
    </div>
</div>
