<div style="background:linear-gradient(0deg, rgba(40, 40, 40, 0.6), rgba(40, 40, 40, 0.6)), url('{{url('assets_front/imgs/conteudos-banner-bg.jfif')}}');
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
            position: relative;
            width: 100%;" class="banner-smaller-height">
    <div class="bg-banner-black d-none d-md-block"></div>

    <div class=" v-center  v-mob-normal container-xl mr-auto ml-auto">
        <div class="social-banner text-center   d-none d-md-block">
            <p class="mt-5">083 3031-9191</p>
            <a style="padding: 6px" class="btn btn-light btn-round m-8" target="_blank" href="tel:83-3031-9191">
                <img src="{{asset('assets_front/imgs/phone-icon.svg')}}" alt="phone icon">
            </a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://wa.me/5583996361415?text=Olá%20tenho%20interesse%20nos%20empreendimentos%20da%20get%20compactos."><ion-icon style="font-size: 24px;" name="logo-whatsapp"></ion-icon></a>
            <div class="vl"></div>
            <a class="btn btn-light btn-round p-1 m-8" target="_blank" href="https://www.instagram.com/getcompactos/"><ion-icon style="font-size: 24px;" name="logo-instagram"></ion-icon></a>
        </div>
        <div class="d-flex justify-content-center">

            <div style="max-width: 541px;" class="mt-md-170">
                <h1 class="text-center text-md-left mx-auto mx-md-0" style="max-width: 541px;">Leia os nossos conteúdos e let’s be Get 
                </h1>
                <p style="max-width: 504px;" class="text-white text-center text-md-left mx-auto mx-md-0">
                    Fique informado sobre o mercado imobiliário, arquitetura e urbanismo, confira dicas decoração e muito mais 
                </p>
            </div>
        </div>
        <div class=" d-md-none">
            <div class="d-flex justify-content-center pb-20">
                <div class="mt-40 mb-50">
                    <p  class="text-white mb-0 ml-2">|</p>
                    <ion-icon style="font-size: 22px" class="text-white" name="chevron-down-outline"></ion-icon>
                </div>
            </div>
        </div>

    </div>

</div>