@component('mail::message')
<h4>Olá, {{$data->name}}</h4><br><br>

<p>Obrigado pelo seu interesse nos empreendimentos da Get Compactos. Nossa equipe comercial entrará em contato com você em até 1 dia útil.</p>

@endcomponent
