@extends('components.layouts.default')


@section('metas')
    <title>Get Compactos - Uma nova forma de viver e investir</title>
    <meta name="description"
        content="Construtora e incorporadora.  Inovação, tecnologia, sustentabilidade e modernidade em forma de imóveis  compactos. Sua liberdade começa aqui. Let’s Get! ">
    <meta name="keywords"
        content="get, get compactos, construtora,  incorporadora, imóveis, loft, compacto, tecnologia, apartamento, alto  padrão, empreendimento imobiliário, joão pessoa, arquitetura, mercado  imobiliário">
    <meta name="robots" content="">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
@endsection


@section('content')
<div class="background-cinza">

    @include('components.partials.navbar')
    @include('components.partials.home.banner-initial')
    @include('components.partials.home.compactos')
    @include('components.partials.home.empreendimentos')
    @include('components.partials.home.conteudo')
    @include('components.partials.home.mapa')
    @include('components.partials.footer')
</div>
@endsection