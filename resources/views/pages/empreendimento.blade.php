@extends('components.layouts.default')

@section('metas')
    <title>Empreendimentos - Get Compactos</title>
    <meta name="description"
    content="Construtora e incorporadora.  Inovação, tecnologia, sustentabilidade e modernidade em forma de imóveis  compactos. Sua liberdade começa aqui. Let’s Get! ">
    <meta name="keywords"
    content="get, get compactos, construtora,  incorporadora, imóveis, loft, compacto, tecnologia, apartamento, alto  padrão, empreendimento imobiliário, joão pessoa, arquitetura, mercado  imobiliário">
    <meta name="robots" content="">
    <meta name="revisit-after" content="1 day">
    <meta name="language" content="Portuguese">
    <meta name="generator" content="N/A">
    <meta http-equiv="Content-Type" content="text/html;  charset=utf-8">
@endsection

@section('content')
    @include('components.partials.navbar')
    @include('components.partials.empreendimentos.banner_interno')
    @include('components.partials.empreendimentos.oprojeto')
    @include('components.partials.empreendimentos.fotos')
    @include('components.partials.empreendimentos.monteoseu_smaller')
    @include('components.partials.empreendimentos.book')
    @include('components.partials.empreendimentos.andamento')
    @include('components.partials.empreendimentos.map')
    @include('components.partials.empreendimentos.contact')
    @include('components.partials.footer')
@endsection
