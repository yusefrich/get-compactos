<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
    @if($errors->has('title'))
      <span class="text-danger">{{ $errors->first('title') }}</span>
    @endif
  </div>
</div>


<div class="col-sm-12 col-md-8">
  <div class="form-group mb-12">
    {!! Form::label('subtitle', 'Sub título', ['class' => '']) !!}
    {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control required']) !!}
    @if($errors->has('subtitle'))
      <span class="text-danger">{{ $errors->first('subtitle') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img1', 'Imagem background', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img1) <img src="{{url('storage/feat/'.$data->img1)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img1', old('img1'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img2', 'Imagem destaque', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img2) <img src="{{url('storage/feat/'.$data->img2)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img2', old('img2'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img3', 'Imagem logo', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img3) <img src="{{url('storage/feat/'.$data->img3)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img3', old('img3'), ['class' => 'form-control']) !!}
  </div>
</div>
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('bt_title', 'Botão Título', ['class' => '']) !!}
    {!! Form::text('bt_title', old('bt_title'), ['class' => 'form-control required']) !!}
    @if($errors->has('bt_title'))
      <span class="text-danger">{{ $errors->first('bt_title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('bt_url', 'Botão URL', ['class' => '']) !!}
    {!! Form::text('bt_url', old('bt_url'), ['class' => 'form-control required']) !!}
    @if($errors->has('bt_url'))
      <span class="text-danger">{{ $errors->first('bt_url') }}</span>
    @endif
  </div>
</div>



@section('scripts')
  <script src="http://vip.ufcode.com.br/ckeditor/ckeditor.js"></script>
@endsection
