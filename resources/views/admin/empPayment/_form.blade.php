<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('desc', 'Descrição', ['class' => '']) !!}
    {!! Form::text('desc', old('desc'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('entry', 'Entrada(%)', ['class' => '']) !!}
    {!! Form::number('entry', old('entry'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('semester', 'Semestrais(%)', ['class' => '']) !!}
    {!! Form::number('semester', old('semester'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('montly', 'Mensalidades(%)', ['class' => '']) !!}
    {!! Form::number('montly', old('montly'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-3">
  <div class="form-group mb-3">
    {!! Form::label('financy', 'Financiamento(%)', ['class' => '']) !!}
    {!! Form::number('financy', old('financy'), ['class' => 'form-control', 'required']) !!}
  </div>
</div>

