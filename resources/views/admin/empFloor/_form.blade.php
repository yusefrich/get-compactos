@php 
  $img = isset($data) ? [] : ['required']; 
@endphp
<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('name', 'Nome', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    {!! Form::file('img', $img) !!}
  </div>
</div>


