<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('title', 'Título', ['class' => '']) !!}
    {!! Form::text('title', old('title'), ['class' => 'form-control required']) !!}
    @if($errors->has('title'))
      <span class="text-danger">{{ $errors->first('title') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img1', 'Imagem 1', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img1)
      <img src="{{url('storage/home/'.$data->img1)}}" width="50px" height="30px" style="margin-bottom: 5px;">
    @endif
    {!! Form::file('img1', old('img1'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img2', 'Imagem 2', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img2)
      <img src="{{url('storage/home/'.$data->img2)}}" width="50px" height="30px" style="margin-bottom: 5px;">
    @endif
    {!! Form::file('img2', old('img2'), ['class' => 'form-control']) !!}
  </div>
</div>

<div class="col-sm-12 col-md-12">
  <div class="form-group mb-12">
    {!! Form::label('subtitle', 'Sub título', ['class' => '']) !!}
    {!! Form::textarea('subtitle', old('subtitle'), ['class' => 'form-control required']) !!}
    @if($errors->has('subtitle'))
      <span class="text-danger">{{ $errors->first('subtitle') }}</span>
    @endif
  </div>
</div>
