@extends('admin.layouts.default')

@section('content')

<section class="section">
  <div class="section-header">
    <h1>Contato</h1>
  </div>

  <div class="section-body">
    <div class="row mt-4">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h4>
              {{$data->name}}
            </h4>
          </div>
          <div class="card-body">
            <h6>Email: {{$data->mail}}</h6>
            <h6>Telefone: {{$data->phone}}</h6>
            <h6>Tipo: {{$data->type}}</h6>
            <h6>Empreendimento: {{ $data->emp ? $data->emp->ch_title : '' }}</h6>
            <h6>Mensagem: {{$data->msg}}</h6>
            @if($data->type == 'simu')
            <h6>Pavimento: {{$data->simulation->floor['name']}}</h6>
            <h6>Apartamento: {{$data->simulation->unity['name']}}</h6>
            <h6>Valor Empreendimento: R$ {{number_format($data->simulation->unity['price'], 2, ',', '.') }}</h6>
            <h6>Projeto: {{$data->simulation->project['name']}}</h6>
            <h6>Valor Projeto: R$ {{number_format($data->simulation->project['price'], 2, ',', '.') }}</h6>
            <h6>Eletros: {{ implode(', ', $data->simulation->electro['items'])}}</h6>
            <h6>Valor Eletros: R$ {{number_format($data->simulation->electro['price'], 2, ',', '.') }}</h6>
            @if(!$data->simulation->payment['custom'])
            <h6>Entrada: R$ {{number_format($data->simulation->payment['entry'], 2, ',', '.') }}</h6>
            <h6>Semestrais: R$ {{number_format($data->simulation->payment['semester'], 2, ',', '.') }}</h6>
            <h6>Mensais: R$ {{number_format($data->simulation->payment['montly'], 2, ',', '.') }}</h6>
            <h6>Financiamento: R$ {{number_format($data->simulation->payment['financy'], 2, ',', '.') }}</h6>
            @endif
            <h5>Total: R$ {{number_format($data->simulation->total, 2, ',', '.') }}</h5>
            @endif
          </div>
          <div class="card-footer">
            <a href="{{ route('admin.contact.admin') }}" class="btn btn-success">Voltar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
