@php 
  $img = isset($data) ? [] : ['required']; 
@endphp
<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('name', 'Name', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('img', 'Imagem', ['class' => '']) !!}<br>
    @if(isset($data) && $data->img) <img src="{{url('storage/unity/'.$data->img)}}" width="50px" height="30px" style="margin-bottom: 5px;"> @endif
    {!! Form::file('img', $img) !!}
  </div>
</div>

<div class="col-sm-12 col-md-4">
  <div class="form-group mb-4">
    {!! Form::label('price', 'Valor', ['class' => '']) !!}
    {!! Form::text('price', old('price'), ['class' => 'form-control', 'id' => 'currency', 'required']) !!}
    @if($errors->has('price'))
    <span class="text-danger">{{ $errors->first('price') }}</span>
    @endif
  </div>
</div>
<div class="col-sm-12 col-md-12"></div>
@section('scripts')
  @include('admin.partials._maskmoney')
@endsection
