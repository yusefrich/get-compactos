<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('name', 'Name', ['class' => '']) !!}
    {!! Form::text('name', old('name'), ['class' => 'form-control', 'required']) !!}
    @if($errors->has('name'))
      <span class="text-danger">{{ $errors->first('name') }}</span>
    @endif
  </div>
</div>

<div class="col-sm-12 col-md-6">
  <div class="form-group mb-6">
    {!! Form::label('price', 'Valor', ['class' => '']) !!}
    {!! Form::text('price', old('price'), ['class' => 'form-control', 'id' => 'currency', 'required']) !!}
    @if($errors->has('price'))
    <span class="text-danger">{{ $errors->first('price') }}</span>
    @endif
  </div>
</div>
<br>
@section('scripts')
  @include('admin.partials._maskmoney')
@endsection
