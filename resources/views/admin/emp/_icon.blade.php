<div class="col-sm-12 col-md-2">
  <div class="form-group get-form get-form-icons mb-2">
      {{-- <label for="exampleFormControlSelect1">Example select</label><i class="text-dark icon icon-027-espaçoverde"></i>
      <select class="form-control" id="exampleFormControlSelect1">
        <option value="icon-027-espaçoverde"><p>testeaaa</p></option>
        <option value="icon-026-smarthome"><i class="text-dark icon icon-026-smarthome"></i>teste</option>
        <option value="icon-028-negócios"><i class="text-dark icon icon-028-negócios"></i>teste</option>
        <option value="icon-025-tv"><i class="text-dark icon icon-025-tv"></i>teste</option>
        <option value="icon-025-economia"><i class="text-dark icon icon-025-economia"></i>teste</option>
      </select> --}}
  
    {!! Form::label($name, $txt, ['class' => '']) !!}<br>
    {!! Form::select($name, [
        'icon-027-espaçoverde' => ' &#xea02; icon-027-espaçoverde',/* &#xf042; */
        'icon-026-smarthome' => '&#xea03; icon-026-smarthome',
        'icon-028-negócios' => '&#xea04; icon-028-negócios',
        'icon-025-tv' => '&#xea05; icon-025-tv',
        'icon-025-economia' => '&#xea06; icon-025-economia',
        'icon-023-accumulator' => '&#xea07; icon-023-accumulator',
        'icon-020-elevator' => '&#xea08; icon-020-elevator',
        'icon-024-móvel' => '&#xea09; icon-024-móvel',
        'icon-015-running' => '&#xea0a; icon-015-running',
        'icon-022-smart-home' => '&#xea0b; icon-022-smart-home',
        'icon-021-pool-1' => '&#xea0c; icon-021-pool-1',
        'icon-017-wheelchair' => '&#xea0d; icon-017-wheelchair',
        'icon-019-pool' => '&#xea0e; icon-019-pool',
        'icon-018-bath-tub' => '&#xea0f; icon-018-bath-tub',
        'icon-002-people' => '&#xea10; icon-002-people',
        'icon-016-insurance' => '&#xea11; icon-016-insurance',
        'icon-012-parking' => '&#xea12; icon-012-parking',
        'icon-014-sport-1' => '&#xea13; icon-014-sport-1',
        'icon-013-game-controller' => '&#xea14; icon-013-game-controller',
        'icon-011-cyber' => '&#xea15; icon-011-cyber',
        'icon-008-workspace' => '&#xea16; icon-008-workspace',
        'icon-010-dog' => '&#xea17; icon-010-dog',
        'icon-009-cocktail' => '&#xea18; icon-009-cocktail',
        'icon-007-terrace' => '&#xea19; icon-007-terrace',
        'icon-005-planet-earth' => '&#xea1a; icon-005-planet-earth',
        'icon-004-transport' => '&#xea1b; icon-004-transport',
        'icon-006-medal' => '&#xea1c; icon-006-medal',
        'icon-003-signal' => '&#xea1d; icon-003-signal',
        'icon-031-saladejantar' => '&#xea1e; icon-031-saladejantar',
        'icon-001-sport' => '&#xea1f; icon-001-sport',
        'icon-036-lavanderia' => '&#xea20; icon-036-lavanderia',
        'icon-030-plantas' => '&#xea21; icon-030-plantas',
        'icon-035-cowork' => '&#xea22; icon-035-cowork',
        'icon-034-piscina1' => '&#xea23; icon-034-piscina1',
        'icon-032-espaços' => '&#xea24; icon-032-espaços',
        'icon-033-proximidade' => '&#xea25; icon-033-proximidade',
        'icon-029-investimento' => '&#xea26; icon-029-investimento'
    ], old($name), ['class' => 'form-control custom-select']) !!}
  </div>
</div>
