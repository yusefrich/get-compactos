<?php

namespace App\Http\Controllers;

use App\Emp;
use App\EmpPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmpPaymentController extends Controller
{
    protected $emp;
    protected $empPayment;

    public function __construct(Emp $emp, EmpPayment $ep) {
        $this->middleware('auth');

        $this->emp = $emp;
        $this->empPayment = $ep;
    }

    public function index($emp)
    {
        $data = $this->emp->find($emp)->payments()->paginate();
        
        return view('admin.empPayment.index', compact('data', 'emp'));
    }

    public function create($emp)
    {        
        return view('admin.empPayment.create', compact('emp'));
    }

    public function store($emp, Request $request)
    {
        $input = $request->all();
        $input['qtd_semester'] = 0;
        $input['qtd_montly'] = 0;
        
        $this->emp->find($emp)->payments()->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.emp.payment.index', $emp);
    }

    public function show($emp, $id)
    {
        $data = $this->empPayment->find($id);

        return view('admin.empPayment.edit', compact('data', 'emp'));
    }

    public function update($emp, $id, Request $request)
    {
        $input = $request->all();
            
        $this->empPayment->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.emp.payment.index', $emp);
    }

    public function destroy($emp, $id)
    {
        $this->empPayment->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.emp.payment.index', $emp);
    }
}
