<?php

namespace App\Http\Controllers;

use App\EmpFloorUnity;
use App\EmpFloorUnityPlanned;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmpFloorUnityPlannedController extends Controller
{
    protected $unity;
    protected $planned;

    public function __construct(EmpFloorUnity $efu, EmpFloorUnityPlanned $efup) {
        $this->middleware('auth');
        $this->unity = $efu;
        $this->planned = $efup;
    }

    public function index($unity)
    {
        $data = $this->unity->find($unity)->planned()->paginate();

        return view('admin.empFloorUnityPlanned.index', compact('data', 'unity'));
    }

    public function create($unity)
    {
        return view('admin.empFloorUnityPlanned.create', compact('unity'));
    }

    public function store($unity, Request $request)
    {
        $unity = $this->unity->find($unity);
        $input = $request->all();

        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/planned'), $imageName);
        $input['img'] = $imageName;
        $input['price'] = $this->toUsd($input['price']);

        $unity->planned()->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.planned.index', $unity);
    }

    public function show($unity, $id)
    {
        $data = $this->planned->find($id);

        return view('admin.empFloorUnityPlanned.edit', compact('data', 'unity'));
    }

    public function update($unity, $id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/planned'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        $input['price'] = $this->toUsd($input['price']);
        $input['electros'] = isset($input['electros']) ? $input['electros']: [];
        
        $this->planned->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.planned.index', $unity);
    }

    public function destroy($unity, $id)
    {
        $this->planned->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.planned.index', $unity);
    }
}
