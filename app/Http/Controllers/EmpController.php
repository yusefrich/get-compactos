<?php

namespace App\Http\Controllers;

use App\Emp;
use App\PinCat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class EmpController extends Controller
{
    use SEOToolsTrait;
    protected $emp;

    public function __construct(Emp $emp) {
        $this->middleware('auth')->except(['index', 'slug', 'download', 'contact']);
        $this->emp = $emp;
    }

    public function index(Request $request)
    {
        $q = $request->q ?: 'all';

        $emp = $this->emp->orderBy('id', 'asc');
        if ($q !== 'all') {
            $emp = $emp->whereStatus($q);
            if ($q == 'Portfólio') {
                $emp = $emp->whereStatus('Pronto para morar');
            }
        }
        $emp = $emp->get();

        return view('pages.empreendimentos', compact('emp', 'q'));
    }

    public function slug($slug, Request $request)
    {
        $q = $request->q ?: 'all';
        $data = $this->emp->whereSlug($slug)->first();
        $pins = PinCat::with('addresses')->get()->toArray();

        $photos = $data->photos()->order()->get()->groupBy('type');
        
        $this->seo()->setTitle($data->ch_title);
        $this->seo()->setDescription($data->ch_subtitle);

        $this->seo()->opengraph()->setDescription($data->ch_subtitle);
        $this->seo()->opengraph()->setTitle($data->ch_title);
        $this->seo()->opengraph()->setUrl(url('/conteudos/'.$data->slug));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'pt-br');
        $this->seo()->opengraph()->addProperty('locale:alternate', ['pt-pt', 'en-us']);
        $this->seo()->opengraph()->addImage(url('/storage/emp/'.$data->ch_img), ['height' => 451, 'width' => 800]);

        return view('pages.empreendimento', compact('data', 'photos', 'q', 'pins'));
    }

    public function download($slug)
    {
        $data = $this->emp->whereSlug($slug)->first();
        
        return response()->download(storage_path('app/public/emp/'.$data->book));
    }

    public function contact($slug, Request $request)
    {
        $input = $request->all();

        $data = $this->emp->whereSlug($slug)->first();
        $data->contact()->create($input);

        toastr()->success('Contato Enviado');

        return redirect()->route('emp', $data->slug);
    }


    public function admin()
    {
        $data = $this->emp->paginate();

        return view('admin.emp.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.emp.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['slug'] = Str::slug($input['ch_title'], '-');
        $input['price'] = 0;
        if (isset($input['img1'])) {
            $imageName = time().'.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/emp'), $imageName);

            $input['img1'] = $imageName;
        }

        if (isset($input['background'])) {
            $imageName = time().'back.'.$input['background']->extension();
            $input['background']->move(public_path('storage/emp'), $imageName);

            $input['background'] = $imageName;
        }

        if (isset($input['ch_img'])) {
            $imageName = time().'ch.'.$input['ch_img']->extension();
            $input['ch_img']->move(public_path('storage/emp'), $imageName);

            $input['ch_img'] = $imageName;
        }

        if (isset($input['logo'])) {
            $imageName = time().'logo.'.$input['logo']->extension();
            $input['logo']->move(public_path('storage/emp'), $imageName);

            $input['logo'] = $imageName;
        }

        if (isset($input['img2'])) {
            $imageName = time().'.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/emp'), $imageName);

            $input['img2'] = $imageName;
        }

        if (isset($input['book'])) {
            $imageName = $input['book']->getClientOriginalName();
            $input['book']->move(public_path('storage/emp'), $imageName);

            $input['book'] = $imageName;
        }

        if (isset($input['imgbook'])) {
            $imageName = time().'8.'.$input['imgbook']->extension();
            $input['imgbook']->move(public_path('storage/emp'), $imageName);

            $input['imgbook'] = $imageName;
        }

        if (isset($input['imgPin'])) {
            $imageName = time().'9.'.$input['imgPin']->extension();
            $input['imgPin']->move(public_path('storage/emp'), $imageName);

            $input['imgPin'] = $imageName;
        }
        $arr = [
        'ch_img',
        'ch_title',
        'ch_subtitle',
        'background',
        'logo',
        'head_title',
        'head_subtitle',
        'title',
        'slug',
        'txt',
        'status',
        'img1',
        'img2',
        'column1',
        'column2',
        'icon1',
        'txt1',
        'icon2',
        'txt2',
        'icon3',
        'txt3',
        'icon4',
        'txt4',
        'icon5',
        'txt5',
        'icon6',
        'txt6',
        'icon7',
        'txt7',
        'icon8',
        'txt8',
        'icon9',
        'txt9',
        'icon10',
        'txt10',
        'icon11',
        'txt11',
        'icon12',
        'txt12',
        'icon13',
        'txt13',
        'icon14',
        'txt14',
        'icon15',
        'txt15',
        'book',
        'imgbook',
        'andtxt1',
        'andpct1',
        'andtxt2',
        'andpct2',
        'andtxt3',
        'andpct3',
        'andtxt4',
        'andpct4',
        'andtxt5',
        'andpct5',
        'andtxt6',
        'andpct6',
        'andtxt7',
        'andpct7',
        'andent',
        'andpr',
        'lat',
        'long',
        'imgPin',
        'price'
    ];
    foreach($arr as $field) {
        if (!isset($input[$field])) {
            $input[$field] = '';
        }
    }
    
        $this->emp->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.emp.admin');
    }

    public function show($id)
    {
        $data = $this->emp->find($id);

        return view('admin.emp.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        
        if (isset($input['background'])) {
            $imageName = time().'back.'.$input['background']->extension();
            $input['background']->move(public_path('storage/emp'), $imageName);

            $input['background'] = $imageName;
        } else {
            unset($input['background']);
        }

        if (isset($input['ch_img'])) {
            $imageName = time().'ch.'.$input['ch_img']->extension();
            $input['ch_img']->move(public_path('storage/emp'), $imageName);

            $input['ch_img'] = $imageName;
        } else {
            unset($input['ch_img']);
        }

        if (isset($input['logo'])) {
            $imageName = time().'logo.'.$input['logo']->extension();
            $input['logo']->move(public_path('storage/emp'), $imageName);

            $input['logo'] = $imageName;
        } else {
            unset($input['logo']);
        }

        if (isset($input['img1'])) {
            $imageName = time().'.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/emp'), $imageName);

            $input['img1'] = $imageName;
        } else {
            unset($input['img1']);
        }

        if (isset($input['img2'])) {
            $imageName = time().'.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/emp'), $imageName);

            $input['img2'] = $imageName;
        } else {
            unset($input['img2']);
        }

        if (isset($input['book'])) {
            $imageName = $input['book']->getClientOriginalName();
            $input['book']->move(public_path('storage/emp'), $imageName);

            $input['book'] = $imageName;
        } else {
            unset($input['book']);
        }

        if (isset($input['imgbook'])) {
            $imageName = time().'8.'.$input['imgbook']->extension();
            $input['imgbook']->move(public_path('storage/emp'), $imageName);

            $input['imgbook'] = $imageName;
        } else {
            unset($input['imgbook']);
        }

        if (isset($input['imgPin'])) {
            $imageName = time().'9.'.$input['imgPin']->extension();
            $input['imgPin']->move(public_path('storage/emp'), $imageName);

            $input['imgPin'] = $imageName;
        } else {
            unset($input['imgPin']);
        }
        
        $this->emp->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.emp.admin');
    }

    public function destroy($id)
    {
        $this->emp->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.emp.admin');
    }
}
