<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Home2;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect()->route('admin.home.index');
        //return view('admin.dashboard.index');
    }

    public function home()
    {
        $data = Home2::paginate();

        return view('admin.home2.index', compact('data'));
    }

    public function create()
    {        
        return view('admin.home2.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $imageName = time().'1.'.$input['img1']->extension();
        $input['img1']->move(public_path('storage/home'), $imageName);

        $input['img1'] = $imageName;

        if (isset($input['img2'])) {
            $imageName = time().'2.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/home'), $imageName);

            $input['img2'] = $imageName;
        }
        
        Home2::create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.home.index');
    }

    public function show($id)
    {
        $data = Home2::find($id);

        return view('admin.home2.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img1'])) {
            $imageName = time().'1.'.$input['img1']->extension();
            $input['img1']->move(public_path('storage/home'), $imageName);

            $input['img1'] = $imageName;
        } else {
            unset($input['img1']);
        }

        if (isset($input['img2'])) {
            $imageName = time().'2.'.$input['img2']->extension();
            $input['img2']->move(public_path('storage/home'), $imageName);

            $input['img2'] = $imageName;
        } else {
            unset($input['img2']);
        }
        
        Home2::find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.home.index');
    }

    public function destroy($id)
    {
        Home2::find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.home.index');
    }
}
