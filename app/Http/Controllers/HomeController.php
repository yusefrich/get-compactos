<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Emp;
use App\Home1;
use App\Home2;
use App\PinCat;
use Illuminate\Http\Request;
use Spatie\Sitemap\SitemapGenerator;

class HomeController extends Controller
{
    public function __construct() {
        //$this->middleware('guest');
    }

    public function index(Request $request)
    {
        $home = Home1::all();
        $feat = Home2::all();
        $emp = Emp::inRandomOrder()->limit(3)->get();
        $pins = PinCat::with('addresses')->get()->toArray();
        $blog = Blog::orderBy('id', 'desc')->take(3)->get();
        
        return view('pages.index', compact('home', 'feat', 'emp', 'pins', 'blog'));
    }

    public function notFound()
    {
        return view('pages.404');
    }

    public function sitemap()
    {
        SitemapGenerator::create(config('app.url'))->writeToFile(public_path('sitemap.xml'));
    }
}
