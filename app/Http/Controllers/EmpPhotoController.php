<?php

namespace App\Http\Controllers;

use App\Emp;
use App\EmpPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmpPhotoController extends Controller
{
    protected $emp;
    protected $empPhoto;

    public function __construct(Emp $emp, EmpPhoto $ep) {
        $this->middleware('auth');
        $this->emp = $emp;
        $this->empPhoto = $ep;
    }

    public function index($emp)
    {
        $data = $this->emp->find($emp)->photos()->order()->paginate();

        return view('admin.empPhoto.index', compact('data', 'emp'));
    }

    public function create($emp)
    {        
        return view('admin.empPhoto.create', compact('emp'));
    }

    public function store($emp, Request $request)
    {
        $input = $request->all();

        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/emp'), $imageName);
        $input['img'] = $imageName;
        
        $this->emp->find($emp)->photos()->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.emp.photo.index', $emp);
    }

    public function show($emp, $id)
    {
        $data = $this->empPhoto->find($id);

        return view('admin.empPhoto.edit', compact('data', 'emp'));
    }

    public function update($emp, $id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/emp'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        
        $this->empPhoto->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.emp.photo.index', $emp);
    }

    public function destroy($emp, $id)
    {
        $this->empPhoto->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.emp.photo.index', $emp);
    }
}
