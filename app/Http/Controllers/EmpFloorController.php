<?php

namespace App\Http\Controllers;

use App\Emp;
use App\EmpFloor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmpFloorController extends Controller
{
    protected $emp;
    protected $empFloor;

    public function __construct(Emp $emp, EmpFloor $ef) {
        $this->middleware('auth');

        $this->emp = $emp;
        $this->empFloor = $ef;
    }

    public function index($emp)
    {
        $data = $this->emp->find($emp)->floors()->paginate();
        session(['emp' => $emp]);
        return view('admin.empFloor.index', compact('data', 'emp'));
    }

    public function create($emp)
    {        
        return view('admin.empFloor.create', compact('emp'));
    }

    public function store($emp, Request $request)
    {
        $input = $request->all();

        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/floor'), $imageName);
        $input['img'] = $imageName;
        
        $this->emp->find($emp)->floors()->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.emp.floor.index', $emp);
    }

    public function show($emp, $id)
    {
        $data = $this->empFloor->find($id);

        return view('admin.empFloor.edit', compact('data', 'emp'));
    }

    public function update($emp, $id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/floor'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
            
        $this->empFloor->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.emp.floor.index', $emp);
    }

    public function destroy($emp, $id)
    {
        $this->empFloor->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.emp.floor.index', $emp);
    }
}
