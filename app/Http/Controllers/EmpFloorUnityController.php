<?php

namespace App\Http\Controllers;

use App\Electro;
use App\EmpFloor;
use App\EmpFloorUnity;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmpFloorUnityController extends Controller
{
    protected $floor;
    protected $unity;

    public function __construct(EmpFloor $ef, EmpFloorUnity $efu) {
        $this->middleware('auth');
        $this->floor = $ef;
        $this->unity = $efu;
    }

    public function index($floor)
    {
        $data = $this->floor->find($floor)->unities()->paginate();
        session(['floor' => $floor]);
        return view('admin.empFloorUnity.index', compact('data', 'floor'));
    }

    public function create($floor)
    {
        $electros = Electro::all();

        return view('admin.empFloorUnity.create', compact('floor', 'electros'));
    }

    public function store($floor, Request $request)
    {
        $floor = $this->floor->find($floor);
        $input = $request->all();

        $imageName = time().'.'.$input['img']->extension();
        $input['img']->move(public_path('storage/unity'), $imageName);
        $input['img'] = $imageName;
        $input['price'] = $this->toUsd($input['price']);
        $input['emp_id'] = $floor->emp_id;
        $input['sold'] = isset($input['sold']) ? 1 : 0;
        $floor->unities()->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.unity.index', $floor);
    }

    public function show($floor, $id)
    {
        $data = $this->unity->find($id);
        $electros = Electro::all();

        return view('admin.empFloorUnity.edit', compact('data', 'floor', 'electros'));
    }

    public function update($floor, $id, Request $request)
    {
        $input = $request->all();
        if (isset($input['img'])) {
            $imageName = time().'.'.$input['img']->extension();
            $input['img']->move(public_path('storage/unity'), $imageName);

            $input['img'] = $imageName;
        } else {
            unset($input['img']);
        }
        $input['price'] = $this->toUsd($input['price']);
        $input['electros'] = isset($input['electros']) ? $input['electros']: [];
        $input['sold'] = isset($input['sold']) ? 1 : 0;

        $this->unity->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.unity.index', $floor);
    }

    public function destroy($floor, $id)
    {
        $this->unity->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.unity.index', $floor);
    }
}
