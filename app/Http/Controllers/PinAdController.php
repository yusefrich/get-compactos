<?php

namespace App\Http\Controllers;

use App\PinAd;
use App\PinCat;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PinAdController extends Controller
{
    protected $pin;
    protected $pinAd;

    public function __construct(PinCat $pin, PinAd $ad) {
        $this->middleware('auth');
        $this->pin = $pin;
        $this->pinAd = $ad;
    }

    public function index($pin)
    {
        $data = $this->pin->find($pin)->addresses()->paginate();

        return view('admin.pinAd.index', compact('data', 'pin'));
    }

    public function create($pin)
    {        
        return view('admin.pinAd.create', compact('pin'));
    }

    public function store($pin, Request $request)
    {
        $input = $request->all();
        
        $this->pin->find($pin)->addresses()->create($input);

        toastr()->success('Registro Salvo');

        return redirect()->route('admin.pin.ad.index', $pin);
    }

    public function show($pin, $id)
    {
        $data = $this->pinAd->find($id);

        return view('admin.pinAd.edit', compact('data', 'pin'));
    }

    public function update($pin, $id, Request $request)
    {
        $input = $request->all();
        
        $this->pinAd->find($id)->update($input);

        toastr()->success('Registro Atualizado');

        return redirect()->route('admin.pin.ad.index', $pin);
    }

    public function destroy($pin, $id)
    {
        $this->pinAd->find($id)->delete();

        toastr()->success('Registro Apagado');

        return redirect()->route('admin.pin.ad.index', $pin);
    }
}
