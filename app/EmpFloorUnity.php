<?php

namespace App;

use App\Electro;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EmpFloorUnity extends Model
{
    protected $table = 'emp_floor_unity';

    protected $casts = [
        'electros' => 'array',
    ];

    protected $appends = ['size_slug', 'electros_new'];
    
    protected $fillable = [
        'emp_id',
        'floor_id',
        'size',
        'name',
        'img',
        'price',
        'electros',
        'sold',
        'coordinate',
    ];

    public function planned()
    {
        return $this->hasMany(EmpFloorUnityPlanned::class, 'unity_id');
    }

    public function getElectrosNewAttribute()
    {
        $arr = [];
        if ($this->electros) {
            foreach($this->electros as $electro) {
                $electro = json_decode($electro);
                $arr[] = Electro::find($electro->id)->toJson();
            }
        }
        return $arr;
    }

    public function getSizeSlugAttribute()
    {
        return Str::slug($this->size, '-');
    }
}
