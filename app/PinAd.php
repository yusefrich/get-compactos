<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinAd extends Model
{
    protected $table = 'pin_ad';
    
    protected $fillable = [
        'cat_id',
        'name',
        'lat',
        'long',
    ];
}
