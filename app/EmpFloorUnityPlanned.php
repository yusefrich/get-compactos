<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpFloorUnityPlanned extends Model
{
    protected $table = 'emp_floor_unity_planned';
    
    protected $fillable = [
        'unity_id',
        'name',
        'img',
        'price'
    ];
}
