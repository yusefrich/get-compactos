<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eletro extends Model
{
    protected $table = 'emp_floor_unity';
    
    protected $fillable = [
        'emp_id',
        'floor_id',
        'name',
        'img',
        'price'
    ];
}
