<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactSimulation extends Model
{    
    protected $fillable = [
        'contact_id',
        'emp_id',
        'floor',
        'unity',
        'project',
        'electro',
        'payment',
        'total'
    ];

    protected $casts = [
        'floor'   => 'array',
        'unity'   => 'array',
        'project' => 'array',
        'electro' => 'array',
        'payment' => 'array',
    ];
}
