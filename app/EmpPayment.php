<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpPayment extends Model
{
    protected $table = 'emp_payments';
    
    protected $fillable = [
        'emp_id',
        'title',
        'desc',
        'entry',
        'semester',
        'qtd_semester',
        'montly',
        'qtd_montly',
        'financy'
    ];
}
